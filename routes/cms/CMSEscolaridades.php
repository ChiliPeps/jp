<?php

//Escolaridades Module
Route::get('escolaridades',   ['as' => 'admin.escolaridades.index', 'uses' => 'Cms\EscolaridadesController@index']);

//Ajax Routes
Route::group(['middleware' => ['ajax']], function () {

	Route::get('escolaridad/getAll',       ['as' => 'admin.escolaridad.getData',      'uses' => 'Cms\EscolaridadesController@getAll']);

	});