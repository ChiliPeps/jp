<?php

//Reportes Module
Route::get('reportes',   ['as' => 'admin.reportes.index', 'uses' => 'Cms\ReportesController@index']);

Route::get('corte/{fecha}/{fecha2}',   ['as' => 'admin.reportes.corte', 'uses' => 'Cms\ReportesController@corteDetallePdf']);

Route::get('corteN/{fecha}/{fecha2}',   ['as' => 'admin.reportes.corte', 'uses' => 'Cms\ReportesController@cortePdf']);