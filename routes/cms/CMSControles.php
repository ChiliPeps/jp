<?php

//Controles Module
Route::get('controles',   ['as' => 'admin.controles.index', 'uses' => 'Cms\ControlesController@index']);

//Ajax Routes
Route::group(['middleware' => ['ajax']], function () {

	Route::get('control/getAlumnos',       ['as' => 'admin.control.getAlumnos',      'uses' => 'Cms\ControlesController@getAlumnos']);

	Route::post('control/updateEscolaridad',  ['as' => 'admin.control.updateEscolaridad',      'uses' => 'Cms\ControlesController@updateEscolaridades']);

	Route::get('control/borrar',       ['as' => 'admin.control.borrar',      'uses' => 'Cms\ControlesController@borrarVentas']);

	Route::get('control/excel', ['as' => 'admin.control.getExcel', 'uses' => 'Cms\ControlesController@getExcel']);
	Route::post('control/subirExcel', ['as' => 'admin.control.subir', 'uses' => 'Cms\ControlesController@uploadExcel']);

	Route::get('control/borrarAlumnosEscolaridad',  ['as' => 'admin.control.borrarAlumnosEscolaridad',      'uses' => 'Cms\ControlesController@borrarAlumnosEscolaridad']);

	Route::get('control/borrarAllAlumnos',  ['as' => 'admin.control.borrarTodosAlumnos',      'uses' => 'Cms\ControlesController@borrarTodosAlumnos']);
	});