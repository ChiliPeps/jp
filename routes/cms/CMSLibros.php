<?php

//Libros Module
Route::get('libros',   ['as' => 'admin.libros.index', 'uses' => 'Cms\LibrosController@index']);

//Ajax Routes
Route::group(['middleware' => ['ajax']], function () {

	Route::get('libros/getLibros',    ['as' => 'admin.libros.getData',  'uses' => 'Cms\LibrosController@getAllLibros']);
	Route::post('libros/save',        ['as' => 'admin.libro.save',      'uses' => 'Cms\LibrosController@saveLibro']);
	Route::post('libros/update',      ['as' => 'admin.libro.update',      'uses' => 'Cms\LibrosController@updateLibro']);
	Route::get('libros/borrar',       ['as' => 'admin.libro.borrar',      'uses' => 'Cms\LibrosController@borrarLibro']);

	Route::get('libros/vendidos',     ['as' => 'admin.libros.vendidos',      'uses' => 'Cms\ReportesController@librosVendidos']);	

	});