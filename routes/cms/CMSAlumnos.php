<?php

//Alumnos Module
Route::get('alumnos',   ['as' => 'admin.alumnos.index', 'uses' => 'Cms\AlumnosController@index']);

//Ajax Routes
Route::group(['middleware' => ['ajax']], function () {

	Route::get('alumnos/get',       ['as' => 'admin.alumnos.get',      'uses' => 'Cms\AlumnosController@getAlumnos']);
	Route::post('alumnos/save',     ['as' => 'admin.alumno.save',      'uses' => 'Cms\AlumnosController@saveAlumno']);
	Route::post('alumnos/update',   ['as' => 'admin.alumno.update',      'uses' => 'Cms\AlumnosController@updateAlumno']);
	Route::get('alumnos/borrar',    ['as' => 'admin.alumno.borrar',      'uses' => 'Cms\AlumnosController@borrarAlumno']);

	Route::get('alumnos/faltantes', ['as' => 'admin.alumnos.faltantes',  'uses' => 'Cms\AlumnosController@faltantes']);
	});