<?php

//Ventas Module
Route::get('ventas',   ['as' => 'admin.ventas.index', 'uses' => 'Cms\VentasController@index']);

Route::get('recibo/venta/{id}', 		['as' => 'admin.ventas.reciboPdf',     'uses' => 'Cms\VentasController@generarPDF']);

Route::get('recibo/recuperar/{id}', 		['as' => 'admin.ventas.recuperarPdf',   'uses' => 'Cms\VentasController@recuperarPDF']);



//Ajax Routes
Route::group(['middleware' => ['ajax']], function () {

	Route::get('alumnos/getLibros', ['as' => 'admin.alumnos.getLibros',    'uses' => 'Cms\VentasController@getLibros']);

	Route::post('ventaLibros', 		['as' => 'admin.ventas.ventaLibros',   'uses' => 'Cms\VentasController@ventaLibro']);

	Route::get('ventas/getAll',		['as' => 'admin.ventas.getData',    'uses' => 'Cms\VentasController@getVentas']);

	Route::get('ventas/getCorte',		['as' => 'admin.ventas.getCorte',    'uses' => 'Cms\VentasController@getCorte']);
	
	Route::get('alumnosVender/get',       ['as' => 'admin.alumnos.vender',      'uses' => 'Cms\VentasController@getAlumnosVender']);
	// Route::get('recibo/venta/{idalumno}', 		['as' => 'admin.ventas.reciboPdf',     'uses' => 'Cms\VentasController@generarPDF']);

	Route::delete('venta/delete',    ['as' => 'admin.venta.borrar',      'uses' => 'Cms\VentasController@deleteVenta']);
	
	});