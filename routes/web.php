<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Index
// Route::get('/', function () { return view('home'); });
Route::get('/', function () { return view('cms.auth.login'); });

// //recibo
Route::get('recibo', function () {
 return view('pdf.recibo2'); 
// 	$pdf = PDF::loadView('pdf.recibo');
// 	return $pdf->download('recibo.pdf');
});

//Auth Routes
Auth::routes();

//User Home/Dashboard
Route::get('/home', 'HomeController@index');