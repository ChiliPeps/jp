<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsLibrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_libros', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->integer('stock');
            $table->float('precio');
            $table->string('escolaridad');
            $table->string('grado');
            $table->string('especialidad')->nullable();
            // $table->float('iva');

            // $table->integer('id_escolaridad')->unsigned();
            // $table->foreign('id_escolaridad')->references('id')->on('cms_escolaridades');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_libros');
    }
}
