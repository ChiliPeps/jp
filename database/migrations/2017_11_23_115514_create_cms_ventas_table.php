<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsVentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_ventas', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('vendedor');
            $table->float('total');
            $table->float('pagoCon');
            $table->integer('id_alumno')->unsigned();
            $table->foreign('id_alumno')->references('id')->on('cms_alumnos');

            $table->timestamps();
            $table->softDeletes();
        });

         Schema::create('cms_venta_detalle', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('id_venta')->unsigned();
            $table->foreign('id_venta')->references('id')->on('cms_ventas');

            $table->integer('id_libro')->unsigned();
            $table->foreign('id_libro')->references('id')->on('cms_libros');

            $table->float('precio_libro');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_ventas');
        Schema::dropIfExists('cms_venta_detalle');
    }
}
