
<html lang="en">
<style>
.clearfix:after {
  content: "";
  display: table;
  clear: both;
}

a {
  color: #5D6975;
  text-decoration: underline;
}

body {
  position: relative;
  /*width: 21cm;  */
  /*height: 29.7cm; */
  margin: 0 auto; 
  color: #001028;
  /*background: #FFFFFF; */
  font-family: Arial, sans-serif; 
  font-size: 12px; 
  font-family: Arial;
}

header {
  padding: 10px 0;
  margin-bottom: 30px;
}

#logo {
  text-align: center;
  margin-bottom: 10px;
}

#logo img {
  width: 200px;
}

h1 {
  border-top: 1px solid  #5D6975;
  border-bottom: 1px solid  #5D6975;
  color: #5D6975;
  font-size: 2.4em;
  line-height: 1.4em;
  font-weight: normal;
  text-align: center;
  margin: 0 0 20px 0;
  background: url(./img/dimension.png);
}

#project {
  float: left;
}

#project span {
  color: #5D6975;
  text-align: left;
  width: 70px;
  /*margin-right: 10px;*/
  display: inline-block;
  font-size: 0.8em;
}

#company {
  float: center;
  text-align: right;
}

#project div,
#company div {
  white-space: nowrap;        
}

table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 20px;
}

table tr:nth-child(2n-1) td {
  /*background: #F5F5F5;*/
}

table th,
table td {
  text-align: center;
}

table th {
  /*padding: 5px 20px;*/
  color: #5D6975;
  border-bottom: 1px solid #C1CED9;
  /*white-space: nowrap;        */
  font-weight: normal;
}

table .service,
table .desc {
  text-align: center;
}

table td {
  padding: 5px;
  text-align: right;
}

table td.service,
table td.desc {
  vertical-align: top;
}

table td.unit,
table td.qty,
table td.total {
  font-size: 1.2em;
}

table td.grand {
  border-top: 1px solid #5D6975;
}

#notices .notice {
  color: #5D6975;
  font-size: 1.2em;
}

footer {
  color: #5D6975;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #C1CED9;
  padding: 8px 0;
  text-align: center;
}

</style>

<body>
  <?php

    $fecha = $data['fechaEsp'];
    $fecha2= $data['fechaEsp2'];
    $corte = $data['corte'];
    $suma = $data['suma'];
  ?>

  <div class="container">
    <header class="clearfix">
      <div id="logo">
        <img  class="tabula" src="./img/jp2.png"  alt="">
      </div>
      <h1>Corte del {{$fecha}} al {{$fecha2}}</h1>
    </header>

    <main>
      <table style="width:100%">
        <thead>
          <tr>
            <th class="service">Venta</th>
            {{-- <th class="desc"></th> --}}
            <th>ALUMNO</th>
            <th>PRECIO</th>
          </tr>
        </thead>

        <tbody>
          @for ($i=0; $i < count($corte) ; $i++) 
            <tr style = "background: #F5F5F5;">
              <td class="service"> {{ $corte[$i]['id'] }}</td>
              <td class="desc"> {{$corte[$i]['alumno']['nombre']}} {{$corte[$i]['alumno']['apellidoP']}} </td>
              <td> $ {{ $corte[$i]['total'] }}</td>
            </tr>            
          @endfor
          <tr>
            <td colspan="2" class="grand total">Total de ventas</td>
            <td class="grand total">${{$suma[0]['total']}}</td>
          </tr>

        </tbody>
      </table>

    </main>
    <footer>
      texto informativo a cambiar
    </footer>
  </div>
</body>

</html>