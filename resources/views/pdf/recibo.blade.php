
<html lang="en">
<style>
.clearfix:after {
  content: "";
  display: table;
  clear: both;
}

a {
  color: #5D6975;
  text-decoration: underline;
}

body {
  position: relative;
  /*width: 21cm;  */
  /*height: 29.7cm; */
  margin: 0 auto; 
  color: #001028;
  /*background: #FFFFFF; */
  font-family: Arial, sans-serif; 
  font-size: 12px; 
  font-family: Arial;
}

header {
  padding: 10px 0;
  margin-bottom: 8px;
}

#logo {
  text-align: center;
  margin-bottom: 10px;
}

#logo img {
  width: 150px;
}

h1 {
  border-top: 1px solid  #5D6975;
  border-bottom: 1px solid  #5D6975;
  color: #5D6975;
  font-size: 2.4em;
  line-height: 1.4em;
  font-weight: normal;
  text-align: center;
  margin: 0 0 20px 0;
  background: url(./img/dimension.png);
}

#project {
  float: left;
}

#project span {
  color: #5D6975;
  text-align: left;
  width: 70px;
  /*margin-right: 10px;*/
  display: inline-block;
  font-size: 0.8em;
}

#company {
  float: center;
  text-align: right;
}

#project div,
#company div {
  white-space: nowrap;        
}

table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 20px;
}

table tr:nth-child(2n-1) td {
  background: #F5F5F5;
}

table th,
table td {
  text-align: center;
}

table th {
  padding: 5px;
  color: #5D6975;
  border-bottom: 1px solid #C1CED9;
  white-space: nowrap;        
  font-weight: normal;
}

table .service,
table .desc {
  text-align: left;
}

table td {
 
  text-align: right;
}

table td.service,
table td.desc {
  vertical-align: top;
}

table td.unit,
table td.qty,
table td.total {
  font-size: 1.2em;
}

table td.grand {
  border-top: 1px solid #5D6975;;
}

#notices .notice {
  color: #5D6975;
  font-size: 1.2em;
}

footer {
  color: #5D6975;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #C1CED9;
  padding: 8px 0;
  text-align: center;
}

</style>

<body>
  <?php

  $alumno = $data['venta'][0]['alumno']['nombre']." ".$data['venta'][0]['alumno']['apellidoP']." ".$data['venta'][0]['alumno']['apellidoM'];
  $vendedor= $data['venta'][0]['vendedor'];
  $escolaridad = $data['venta'][0]['alumno']['escolaridad']['nivel']." ".$data['venta'][0]['alumno']['escolaridad']['grado']." - ".$data['venta'][0]['alumno']['escolaridad']['grupo'];
  $idVenta = $data['idVenta'];
  $total = $data['venta'][0]['total'];
  $fecha = $data['fechaEsp'];
  $venta = $data['datos'];
  $efectivo = $data['venta'][0]['pagoCon'];

  ?>

  <div class="container">
     <header class="clearfix"> 
      <div id="logo">
        {{--<img  class="tabula" src="./img/jp2.png"  alt=""> --}}
      </div>
      <h1>Folio #{{$idVenta}}</h1>

      <div id="company" class="clearfix">
        <div>Comunidad educativa JPII</div>
        <div>Tel.612-122-0107</div>
        <div>info@juanpablo2.edu.mx</div>
        <div>{{$fecha}}</div>
      </div>

      <div id="project">

        <div><span>Departamento:  </span> Biblioteca</div>
        <div><span>Entrego:  </span>{{$vendedor}}</div>
        <div><span>Alumno: </span>{{$alumno}}</div>
        <div><span>Escolaridad: </span>{{$escolaridad}}</div>

      </div>
    </header>

    <main>
      <table style="width:100%">
        <thead>
          <tr>
            <th class="service">LIBRO</th>
            <th>PRECIO</th>
          </tr>
        </thead>

        <tbody>
          @for ($i=0; $i < count($venta) ; $i++) 
            <tr>
              <td class="service"> {{ $venta[$i]['libro']['nombre'] }}</td>
              {{-- <td class="desc"></td> --}}
              <td class="total">${{$venta[$i]['libro']['precio']}}</td>
            </tr>
          @endfor
          
          <tr>
            <td><br></td>
          </tr>
          
          <tr>
            <td colspan="1" class="grand total">TOTAL</td>
            <td class="grand total">${{$total}}</td>
          </tr>

          <tr>
            <td colspan="1" class="grand total">EFECTIVO</td>
            <td class="grand total">${{$efectivo}}</td>
          </tr>

          <tr>
            <td colspan="1" class="grand total">CAMBIO</td>
            <td class="grand total">${{$efectivo - $total}}</td>
          </tr>

        </tbody>
      </table>

   </main>
    <footer>

    </footer>

    <!--copia-->
    <header class="clearfix"> 
        <div id="logo">
          {{--<img  class="tabula" src="./img/jp2.png"  alt=""> --}}
        </div>
        <h1>Folio #{{$idVenta}}</h1>
  
        <div id="company" class="clearfix">
          <div>Comunidad educativa JPII</div>
          <div>Tel.612-122-0107</div>
          <div>info@juanpablo2.edu.mx</div>
          <div>{{$fecha}}</div>
        </div>
  
        <div id="project">
  
          <div><span>Departamento:  </span> Biblioteca</div>
          <div><span>Entrego:  </span>{{$vendedor}}</div>
          <div><span>Alumno: </span>{{$alumno}}</div>
          <div><span>Escolaridad: </span>{{$escolaridad}}</div>
  
        </div>
      </header>
  
      <main>
        <table style="width:100%">
          <thead>
            <tr>
              <th class="service">LIBRO</th>
              <th>PRECIO</th>
            </tr>
          </thead>
  
          <tbody>
            @for ($i=0; $i < count($venta) ; $i++) 
              <tr>
                <td class="service"> {{ $venta[$i]['libro']['nombre'] }}</td>
                {{-- <td class="desc"></td> --}}
                <td class="total">${{$venta[$i]['libro']['precio']}}</td>
              </tr>
            @endfor
            
            <tr>
              <td><br></td>
            </tr>
            
            <tr>
              <td colspan="1" class="grand total">TOTAL</td>
              <td class="grand total">${{$total}}</td>
            </tr>
  
            <tr>
              <td colspan="1" class="grand total">EFECTIVO</td>
              <td class="grand total">${{$efectivo}}</td>
            </tr>
  
            <tr>
              <td colspan="1" class="grand total">CAMBIO</td>
              <td class="grand total">${{$efectivo - $total}}</td>
            </tr>
  
          </tbody>
        </table>
  
     </main>
      <footer>
  
      </footer>
  </div>
</body>

</html>