{{-- Requerimientos: Bootstrap 3, Vue.js 2, VueHelperFunctions.blade.php -> Lodash.js, Laravel 5.3 --}}
{{-- Requiere Componente vueDateRangePicker antes de inicializar este. --}}
<script>
Vue.component('datefiltersearch', {
    props: ['selected', 'options'],
    template: `
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="busqueda">Busqueda</label>
                <input label="Busqueda" name="busqueda" type="text" class="form-control" v-model="busqueda" placeholder="{{trans('cms.search_bar')}}">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="tipo">Por</label>
                <select id="tipo" name="tipo" class="form-control" v-model="tipo" @change="filterSearch_tipo">
                    <option v-for="o in options" :value="o.value">@{{ o.text }}</option>
                </select>
            </div>
        </div>
        <div class="col-md-3" v-if="dateRangeActive">
            <label>Rango Fecha</label>
            <daterangepicker v-model="fechas"></daterangepicker>
        </div>
        <div class="col-md-3" style="padding-top: 24px;">
            <button class="btn btn-grey" @click="toggleDateRangePicker">@{{textButtonDateRange}}</button>
        </div>
    </div>
    `,
    mounted: function () {
        this.tipo = this.selected;
    },
    data: function () {
        return {
            tipo: '',
            busqueda: '',
            fechas: '',
            dateRangeActive: false,
            textButtonDateRange: "Agregar Filtrar por Fechas"
        }
    },
    watch: {
        busqueda: function () {
            this.filterSearch();
        },
        fechas: function (val) {
            this.filterSearch_fechas();
        }
    },
    methods: {
        //Filter Search
        filterSearch: _.debounce(function () {
            this.emitUpdateFilters();
        }, 500),

        filterSearch_tipo: _.debounce(function () {
            if(this.busqueda == '') { return; }
            this.emitUpdateFilters();
        }, 500),

        filterSearch_fechas: _.debounce(function () {
            if(this.fechas == '') { return; }
            this.emitUpdateFilters();
        }, 500),

        emitUpdateFilters: function () {
            this.$emit("updatefilters", { busqueda: this.busqueda, tipo: this.tipo, fechas: this.fechas }); 
        },

        toggleDateRangePicker: function () {
            this.dateRangeActive = !this.dateRangeActive;
            if(this.textButtonDateRange == "Agregar Filtrar por Fechas") {
                this.textButtonDateRange = "Quitar Filtro por Fechas";
            } else {
                this.fechas = '';
                this.emitUpdateFilters();
                this.textButtonDateRange = "Agregar Filtrar por Fechas";
            }
        }
    }
});
</script>