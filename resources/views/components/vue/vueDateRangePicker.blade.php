@push('css')
{!! Html::style('plugins/bootstrap-daterangepicker/daterangepicker.css') !!}
@endpush

{{-- Bootstrap DateRangePicker --}}
{!! Html::script('plugins/bootstrap-daterangepicker/moment.min.js') !!}
{!! Html::script('plugins/bootstrap-daterangepicker/daterangepicker.js') !!}

<script>
//Variable Format { startDate: '03/05/2005', endDate: '03/06/2005' }
Vue.component('daterangepicker', {
    props: ['value'],
    template: `<input ref='daterange' type="text" class="form-control" readonly>`,
    mounted: function () {
        var vm = this;

        $(vm.$refs.daterange).daterangepicker({
            locale: this.locale_es
        }).on('apply.daterangepicker', function(ev, picker) {
            //Variable Format { startDate: '03/05/2005', endDate: '03/06/2005' }
            var first = picker.startDate.format('YYYY-MM-DD');
            var end = picker.endDate.format('YYYY-MM-DD');
            var selectedRange = { startDate: first, endDate: end };
            $(vm.$refs.daterange).val(vm.customDisplay(selectedRange.startDate, selectedRange.endDate));
            vm.$emit('input', selectedRange);
            //console.log(picker.startDate.format('YYYY-MM-DD'));
            //console.log(picker.endDate.format('YYYY-MM-DD'));
        }).on('cancel.daterangepicker', function(ev, picker) {
            var first = picker.startDate.format('YYYY-MM-DD');
            var end = picker.endDate.format('YYYY-MM-DD');
            var selectedRange = { startDate: first, endDate: end };
            $(vm.$refs.daterange).val(vm.customDisplay(selectedRange.startDate, selectedRange.endDate));
        }).on('hide.daterangepicker', function(ev, picker) {
            var first = picker.startDate.format('YYYY-MM-DD');
            var end = picker.endDate.format('YYYY-MM-DD');
            var selectedRange = { startDate: first, endDate: end };
            $(vm.$refs.daterange).val(vm.customDisplay(selectedRange.startDate, selectedRange.endDate));
        });

        var tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
        var localISOTime = (new Date(Date.now() - tzoffset)).toISOString().slice(0,-1).slice(0, 10);
        $(vm.$refs.daterange).val(vm.customDisplay(localISOTime, localISOTime));
    },
    data: function () {
        return {
            locale_es: {
                "separator": " - ",
                "applyLabel": "Aplicar",
                "cancelLabel": "Cancelar",
                "fromLabel": "Desde",
                "toLabel": "A",
                "customRangeLabel": "Custom",
                "weekLabel": "S",
                "daysOfWeek": ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                "monthNames": ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", 
                "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                "firstDay": 1
            }
        }
    },
    watch: {
        value: function (value, oldValue) {
            // update value
            //Variable Format { startDate: '03/05/2005', endDate: '03/06/2005' }
            if(value != null || value != '') {
                var startDate = value['startDate'].substring(5, 7)+"/"+value['startDate'].substring(8, 10)+"/"+value['startDate'].substring(0, 4);
                var endDate = value['endDate'].substring(5, 7)+"/"+value['endDate'].substring(8, 10)+"/"+value['endDate'].substring(0, 4);

                $(this.$refs.daterange).data('daterangepicker').setStartDate(startDate);
                $(this.$refs.daterange).data('daterangepicker').setEndDate(endDate);

                $(this.$refs.daterange).val(this.customDisplay(value['startDate'], value['endDate']));
            }
        }
    },
    methods: {
        customDisplay: function (start, end) {
            return this.dateToString(start) + ' - ' + this.dateToString(end);
        },

        dateToString: function (fecha) {
            if(fecha == null){return;}
            var meses = new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
            var f = new Date(fecha.replace(/-/g,"/")); //Fix UTC "-" to Local "/"
            return f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear();
        }
    }
});
</script>