@extends('cms.master')
<style>
	/*loading*/
    .cargando{
        width: 100%;
        height:100%;
        position:absolute;
        background:rgba(255,255,255,0.7);
        left:0;
        z-index: 1;
    }
/*    .iconoload{
        position:fixed;
        top:50%;
    }*/
</style>
@section('content')
    <section class="content-header">
        <h1><i class="fa fa-graduation-cap"></i> Alumnos </h1>
    </section>

    <section class="content" id="app" v-cloak>

    	<div class="box box-primary" v-show="panelIndex">
 
        	<div class="box-header">

        		<filtersearch :selected="tipo" :options="tipos" @updatefilters="updateFilters"></filtersearch>
        		 <div class="box-tools pull-right">
                	<button  class="btn btn-block bg-navy" @click="crearNuevo"><i class="fa fa-plus-circle"></i> Crear nuevo alumno</button> 
            	</div>

    		</div>

        	<div class="box-body">

				<!-- VueLoading icon -->
				<div class="text-center cargando" v-show="loading">
                        <img src="../img/komvacHorizontalFlip.gif">
                </div>

				<div class="table-responsive" > {{-- v-show="!loading" --}}
					<table class="table table-bordered table-hover">
					    <thead>
					        <tr>

					            <th>Alumno</th>
					            <th>Escolaridad</th>
					            <th></th>
					            <th></th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr v-for="a in alumnos">
					            {{-- <td><img :src="public_url+paciente.foto+'?'+Date.now()" class="img-rounded" style="width:32px; height:32px;"></td> --}}
					            <td>@{{a.nombre}} @{{a.apellidoP}} @{{a.apellidoM}}</td>				           
					            <td>@{{a.escolaridad.nivel}} @{{a.escolaridad.grado}} - @{{a.escolaridad.grupo}}</td>
					            <td></td>

					            <td>
					                <button type="button" class="btn btn-xs btn-warning" @click="loadPanel(a.id)">
					                <i class="fa fa-refresh"></i> Editar</button>
					                <button type="button" class="btn btn-xs btn-danger" @click="borrarAlumno(a.id)">
					                <i class="fa fa-trash"> Borrar</i></button>
					            </td>
					        </tr>
					    </tbody>
					</table>
				</div>
			</div>
			<div class="box-footer clearfix">
                <pagination @setpage="getData" :param="pagination"></pagination>
            </div>
		</div>
		
		    @include('cms.alumnos.partials.inputs')
    </section>
@endsection

@push('scripts')
    @include('cms.alumnos.partials.scripts')
@endpush