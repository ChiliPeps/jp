<!-- Vue Components & Mixins -->
@include('components.vue.vuePagination')
@include('components.vue.vueHelperFunctions')
@include('components.vue.vueFormErrors')
@include('components.vue.vueNotifications')
<!-- Vue Components & Mixins -->
@include('components.vue.vueFilterSearch')

<script>
//Laravel's Token
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

var app = new Vue ({
    mounted: function () {
    	this.getData(this.dataRoute), this.getEscolaridades();
    },

    el: "#app",

    mixins: [helperFunctions, notifications],

    data: {

    	pagination:null,
    	alumnos:null,
    	escolaridades:null,
    	//Routes
    	dataRoute:"{{route('admin.alumnos.get')}}",

    	panelIndex:true,
    	panelAlumno:false,
    	//search
    	tipo:'nombre',
    	tipos:[
            { text: 'Nombre', value:'nombre' },
            { text:'Apellido Paterno', value:'apellidoP'},
            { text:'Apellido Materno', value:'apellidoM'}
        ],

        alumno:{nombre:'', id_escolaridad:'', apellidoP:'', apellidoM:''},
        botonsave:true,
        botonupdate:false,

        //Errors
        erroresAlumnos:null,

        loading:false,
        disable:false,
    },

    methods: {
    	getData: function (url) {
            this.loading = true;
            var filter = { busqueda: this.busqueda, tipo: this.tipo };
            var resource = this.$resource(url);
            resource.get(filter).then(function (response) {
                this.pagination = response.data;
                this.alumnos = response.data.data;
                this.disable = false,
                this.loading = false;
            });
        },

        getEscolaridades:function(){
            var resource = this.$resource("{{route('admin.escolaridad.getData')}}");
            resource.get().then(function (response) {
                 this.escolaridades = response.data;
            });
        },

        updateFilters: function (data) {
            this.busqueda = data.busqueda;
            this.tipo = data.tipo;
            this.getData(this.dataRoute);
        },

        crearNuevo:function (){
        	this.panelIndex  = false;
        	this.panelAlumno = true;
        },

        regresar:function(){
        	this.panelIndex  = true;
        	this.panelAlumno = false;
            this.botonsave   = true;
            this.botonupdate = false;
            this.alumno = {nombre:'', id_escolaridad:'', apellidoP:'', apellidoM:''};
            this.erroresAlumnos = null;
        },
        addAlumno:function(){
            this.disable = true,
        	this.loading = true;
        	var form = new FormData();
        	form.append('nombre',       		this.alumno.nombre);
        	form.append('id_escolaridad',       this.alumno.id_escolaridad);
        	form.append('apellidoP',      			this.alumno.apellidoP);
        	form.append('apellidoM',       		this.alumno.apellidoM);

        	var resource = this.$resource("{{route('admin.alumno.save')}}");
        	resource.save(form).then(function (response) {
                this.loading = false;
                this.disable = false,
                this.notification('fa fa-check-circle', 'OK!', "Alumno Agregado!", 'topCenter');
                this.regresar();
                this.getData(this.dataRoute);
                // this.disable = false;
            }, function (response) {
                // this.loadingSaveCita = false;
                // this.saveInAction = false;
                this.erroresAlumnos = response.data;
                this.loading = false;
                this.disable = false,
                // this.disable = false;
                this.notification('fa fa-exclamation-triangle', 'Error', "Se ha producido un error.", 'topCenter');
            });
        },

        loadPanel:function(idRecord){
            this.crearNuevo();
            this.botonsave = false;
            this.botonupdate = true;

            var index = this.findIndexByKeyValue(this.alumnos, "id", idRecord);
            this.alumno = {
                id:                 this.alumnos[index].id,
                nombre:             this.alumnos[index].nombre,
                id_escolaridad:     this.alumnos[index].id_escolaridad,
                apellidoP:          this.alumnos[index].apellidoP,
                apellidoM:          this.alumnos[index].apellidoM,
            }
        },

        updateAlumno:function(){
            this.disable = true;
            this.loading= true;
            var form = new FormData();
            form.append('id', this.alumno.id);
            form.append('nombre', this.alumno.nombre);
            form.append('id_escolaridad', this.alumno.id_escolaridad);
            form.append('apellidoP', this.alumno.apellidoP);
            form.append('apellidoM', this.alumno.apellidoM);
           
            var resource = this.$resource("{{route('admin.alumno.update')}}");
            resource.save(form).then(function (response) {
                console.log(response);
                // this.loadingSaveCita = false;
                // this.saveInAction = false;
                this.loading = false,
                this.disable = false,
                this.notification('fa fa-check-circle', 'OK!', "Alumno actualizado!", 'topCenter');
                this.regresar();
                this.getData(this.dataRoute);
            }, function (response) {
                // this.loadingSaveCita = false;
                // this.saveInAction = false;
                this.loading = false,
                this.disable = false,
                this.erroresAlumnos = response.data;
                this.notification('fa fa-exclamation-triangle', 'Error', "Se ha producido un error.", 'topCenter');
            });
        },

        borrarAlumno:function(idRecord){
           swal({
            title: '¿Estas seguro?',
            text: "Esta a punto de borrar al Alumno!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Si, Borralo!',
            cancelButtonText: 'Cancelar'
            }).then(function () {
                var resource = this.$resource("{{route('admin.alumno.borrar')}}");
                var data = {id:idRecord}
                resource.get(data).then(function (response) {
                    this.notification('fa fa-check-circle', 'OK!', "Alumno Borrado!.", 'topCenter');
                    this.getData(this.dataRoute);
                });
            }.bind(this)).catch(swal.noop);
        },
    }
});
</script>