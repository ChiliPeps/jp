<div class="box box-primary" v-show="panelAlumno">
	<div class="box-header">
        <h3 class="box-title">Datos del Alumno</h3>
   
        <div class="box-tools pull-right">
            <button @click="regresar" class="btn"><i class="fa fa-chevron-circle-left"></i> @lang('Regresar')</button>
            <button v-show = "botonsave" @click="addAlumno()" v-bind:disabled = "disable" class="btn bg-navy"><i class="fa fa-floppy-o" ></i> Guardar
                {{-- <i v-show="loadingNoticia" class="fa fa-spinner fa-spin"></i> --}}
            </button>
            <button v-show = "botonupdate" @click="updateAlumno()" class="btn bg-navy"><i class="fa fa-floppy-o"></i>Actualizar</button>

        </div>

    </div>

    <div class="box-body">

    	<div class="col-md-6">
	    	<div class="form-group">
	            <label for="nombre">Nombre</label>
	            <input label="nombre" name="nombre" type="text" class="form-control" v-model="alumno.nombre" placeholder="" :class="formError(erroresAlumnos, 'nombre', 'inputError')">
	        </div>

            <div class="form-group">
                <label for="Appat">Apellido Paterno</label>
                <input label="Appat" name="ApellidoP" type="text" class="form-control" v-model="alumno.apellidoP" placeholder="" :class="formError(erroresAlumnos, 'apellidoP', 'inputError')">
            </div>

            <div class="form-group">
                <label for="Apmat">Apellido Materno</label>
                <input label="Apmat" name="apellidoM" type="text" class="form-control" v-model="alumno.apellidoM" placeholder="" :class="formError(erroresAlumnos, 'apellidoM', 'inputError')">
            </div>

	        <div class="form-group">
                    <label>Escolaridad</label>
                    <select id="tipo" name="tipo" class="form-control" v-model="alumno.id_escolaridad" :class="formError(erroresAlumnos, 'id_escolaridad', 'inputError')">
                    <option v-for="e in escolaridades" :value="e.id">@{{ e.nivel }} @{{e.grado}}-@{{e.grupo}}</option>
                    </select>
            </div>

        </div>
    </div>

</div>