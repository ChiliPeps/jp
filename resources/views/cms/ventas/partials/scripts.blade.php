<!-- Vue Components & Mixins -->
@include('components.vue.vuePagination')
@include('components.vue.vueHelperFunctions')
@include('components.vue.vueFormErrors')
@include('components.vue.vueNotifications')
<!-- Vue Components & Mixins -->
@include('components.vue.vueFilterSearch')
@include('components.vue.vueDatepicker')

<script>
//Laravel's Token
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

var app = new Vue ({
    mounted: function () {
    	this.getData(this.dataRoute);
    },

    el: "#app",

    mixins: [helperFunctions, notifications],

    data: {
        lista:[],
    	//Data
        idalumnoVender:'',
        idEscolaridad:'',
    	alumnos:null,
    	pagination:null,
        total:0,


    	libros:null,
    	paginationLibros:null,

    	//Routes
        dataRoute: "{{route('admin.alumnos.get')}}",
        //ruta para metodo alumnosvendidos
        //dataRoute2: "{{route('admin.alumnos.vender')}}",
    	librosRoute:"{{route('admin.alumnos.getLibros')}}",

    	//filters
    	busqueda:'',
    	tipo:'nombre',
    	tipos:[
            { text: 'Nombre', value:'nombre' },
            { text: 'Apellido Paterno', value:'apellidoP' },
			{ text: 'Apellido Materno', value:'apellidoM' },

        ],

        busquedaL:'',
    	tipoL:'nombre',
    	tiposL:[
            { text: 'Nombre', value:'nombre' },
            { text: 'Editorial', value:'' },
			{ text: 'otrovalor', value:'' },

        ],
        panelindex:true,
        panelventa:false,

        idVenta:'',
        pagando:'',
        //loadings
        loading:false,

    },

    methods: {
    	getData: function (url) {
            this.loading = true;
            var filter = { busqueda: this.busqueda, tipo: this.tipo };
            var resource = this.$resource(url);
            resource.get(filter).then(function (response) {
                this.pagination = response.data;
                this.alumnos = response.data.data;
                this.loading = false;
            });
        },

        updateFilters: function (data) {
            this.busqueda = data.busqueda;
            this.tipo = data.tipo;
            this.getData(this.dataRoute);
        },

        updateFiltersL:function(data){
        	this.busquedaL = data.busqueda;
            this.tipoL = data.tipo;
            this.getLibros(this.librosRoute);
        },
   
        getDatos:function(alumno)
        {   
            // this.loading = true;
            this.idalumnoVender = alumno.id;
            this.idEscolaridad = alumno.id_escolaridad;
            this.getLibros(this.librosRoute);
        },

        getLibros:function(url){
            // this.idalumnoVender = alumno.id;
            window.scrollTo(0,0);
        	this.panelindex = false;
        	this.panelventa = true;
        	this.loading = true;
        	var resource = this.$resource(url);
            var filter = { busqueda: this.busquedaL, tipo: this.tipoL, id:this.idEscolaridad };
        	resource.get(filter).then(function (response) {
                console.log(response.data);
                this.paginationLibros = response.data;
                this.libros = response.data.data;
                this.loading = false;
            });
        },
       
        regresar:function(){
        	this.panelventa = false;
        	this.panelindex = true;
        	this.libros     = null;  
            this.lista      = [];  
            this.total      = 0;	
        },

        libroaventa:function(id){
            var index = this.findIndexByKeyValue(this.libros, 'id', id);

            this.lista.push({
                id: this.libros[index].id,
                nombre: this.libros[index].nombre,
                precio: this.libros[index].precio,
            })

            var precio = parseFloat(this.libros[index].precio);
            this.total += precio;
            
        },

        quitaLibro:function(idRecord)
        {
            for (var i = 0; i <= this.lista.length - 1; i++) {
                if (this.lista[i].id == idRecord) {
                    this.total = this.total - this.lista[i].precio;
                    this.lista.splice(i,1);
                };
            };

        },

        vender:function()
        {
            if (this.pagando >= this.total) {
            this.loading=true;
            datos = {id:this.idalumnoVender, total:this.total, lista:this.lista, efectivo:this.pagando}
            var resource = this.$resource("{{route('admin.ventas.ventaLibros')}}");
            resource.save(datos).then(function (response) {                
                // this.loadingSaveEspecialidad = false;
                // this.saveInAction = false;
                
                this.notification('fa fa-check-circle', 'OK!', "Venta Registrada!", 'topCenter');
                this.regresar();
                this.lista = [],
                this.total = 0;
                this.pagando = 0;
                this.idVenta = response.data;
                this.prueba(this.idVenta);
                // this.closeAddEspecialidad();
                // this.getEspecialidades(this.dataRoute);
            }, function (response) {
                this.loading=false;
                this.regresar();
                this.lista = [],
                this.total = 0;
                this.pagando = 0;
                this.notification('fa fa-exclamation-triangle', 'Error', "Se ha producido un error.", 'topCenter');
            });
            }
            else{
                this.notification('fa fa-exclamation-triangle', 'Error', "El pago debe ser mayor o igual al monto", 'topCenter');
            }
        },

        prueba:function(idventa){
            window.location = "{{URL::to('admin/recibo/venta')}}/"+idventa;
        },
    }
});
</script>