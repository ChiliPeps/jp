<div class="box box-primary" v-show="panelventa">

	<div class="box-header">
        <h3 class="box-title"></h3>
        <div class="box-tools pull-right">
            <a href="#" class="btn btn-warning" @click="regresar">
                <i class="fa fa-backward"></i> Regresar
            </a>
        </div>

        <filtersearch :selected="tipoL" :options="tiposL" @updatefilters="updateFiltersL"></filtersearch>

    </div>

    		<div class="box-body">

			   <div class="text-center cargando" v-show="loading">
			        <img src="../img/komvacHorizontalFlip.gif">
			    </div>
    
				<div class="col-md-6">
					<div class="table-responsive" > {{-- v-show="!loading" --}}
						<table class="table table-bordered table-hover">
						    <thead>
						        <tr>

						            <th>Nombre</th>
						            <th>Sem</th>
						            <th>Especialidad </th>
						            <th>Precio</th>
						            <th>stock</th>
						         	<th>Total</th>

						        </tr>
						    </thead>
						    <tbody>
						        <tr v-for="l in libros">
						            {{-- <td><img :src="public_url+paciente.foto+'?'+Date.now()" class="img-rounded" style="width:32px; height:32px;"></td> --}}

						            <td>@{{l.nombre}}</td>
						            <td>@{{l.semestre}}</td>
						            <td>@{{l.especialidad}}</td>
						            <td>$ @{{l.precio}}</td>
						            <td>@{{l.stock}}</td>
						            <td>
						                <button v-show="l.stock > 0" type="button" class="btn btn-xs btn-success" @click="libroaventa(l.id)">
						                <i class="fa fa-cart-plus"></i> </button>
						            </td>
						        </tr>
						    </tbody>
						</table>
					</div>
					<div class="box-footer clearfix">
	            	<pagination @setpage="getLibros" :param="paginationLibros"></pagination>
	    			</div>
				</div>

				<div class="col-md-6">

					<table class="table"  v-show = "lista.length > 0">
						    <thead>
						        <tr>

						            <th>Nombre</th>
						            <th>Escolaridad</th>
						            <th>Precio</th>
						            
						            <th></th>
						        </tr>
						    </thead>
						    <tbody>
						        <tr v-for="l in lista" class="success">

						            <td>@{{l.nombre}}</td>
						            <td>@{{l.id_escolaridad}}</td>
						            <td>$ @{{l.precio}}</td>
						            <td>						       
						           		<button type="button" class="btn btn-xs btn-danger" @click="quitaLibro(l.id)"><i class="fa fa-trash"></i></button>						            
						            </td>

						        </tr>

						        <tr  class="warning">
						        	<td> <b>Total</b></td>						            
						            <td></td>
						            <td>@{{total}}</td>
						            <td></td>						        
						        </tr>

						        <tr>
						        	<td><label for="nombre">Pago con:</label></td>
						        	<td>$</td>
						        	<td>
						        		<div class="form-group">
								            <input label="nombre" name="nombre" type="number" class="form-control" v-model="pagando" placeholder="" v-on:keyup.13="vender">
								        </div>
						        	</td>
						        	<td></td>
						        </tr>

						        <tr>
						        	<td> <b>Cambio</b> </td>
						        	<td></td>
						        	<td> <div v-if="pagando >= total"> @{{pagando - total}}</div></td>
						        	<td>
						        		<button  v-if="pagando >= total" type="button" class="btn btn-xs btn-success" @click="vender">Registrar <i class="fa fa-check-square-o"></i></button>
						        	</td>
						        </tr>
						    </tbody> 
						</table>
				</div>

			</div>

    	</div>