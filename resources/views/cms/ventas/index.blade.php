@extends('cms.master')
<style>
	/*loading*/
    .cargando{
        width: 100%;
        height:100%;
        position:absolute;
        background:rgba(255,255,255,0.7);
        left:0;
        z-index: 1;
    }
</style>
@section('content')
    <section class="content-header">
        <h1><i class="fa fa-dollar"></i> Ventas </h1>
    </section>

    <section class="content" id="app" v-cloak>

    	<div class="box box-primary" v-show="panelindex">

    		<div class="box-header">
        		<filtersearch :selected="tipo" :options="tipos" @updatefilters="updateFilters"></filtersearch>
    		</div>

    		<div class="box-body">

				<!-- VueLoading icon -->
				{{-- <div class="text-center"><i v-show="loading" class="fa fa-spinner fa-spin fa-5x"></i></div> --}}

				<div class="table-responsive" > {{-- v-show="!loading" --}}
					<table class="table table-bordered table-hover">
					    <thead>
					        <tr>

					            <th>Nombre</th>
					            <th>Apellidos</th>
					            <th>Escolaridad</th>
					            <th>Email</th>
					            <th>Teléfono</th>
					            <th>Activo</th>
					            <th>Venta</th>
					            <th></th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr v-for="a in alumnos">
					            {{-- <td><img :src="public_url+paciente.foto+'?'+Date.now()" class="img-rounded" style="width:32px; height:32px;"></td> --}}

					            <td>@{{a.nombre}}</td>
					            <td>@{{a.apellidoP}} @{{a.apellidoM}}</td>
					            <td>@{{a.escolaridad.nivel}}-@{{a.escolaridad.grado}} @{{a.escolaridad.grupo}}</td>
					            <td></td>
					            <td>
					{{--                     <span v-if="paciente.activo == 'si'" class="label label-success">Si</span>
					                <span v-if="paciente.activo == 'no'" class="label label-primary">No</span> --}}
					            </td>
					            <td></td>
					            <td>
					                <button type="button" class="btn btn-xs btn-info" @click="getDatos(a)">
					                <i class="fa fa-file-text"></i> Vender</button>
					              {{--   <button type="button" class="btn btn-xs btn-warning" @click="prueba(a.id)">
					                <i class="fa fa-refresh"></i> Editar</button>
					                <button type="button" class="btn btn-xs btn-danger" @click="">
					                <i class="fa fa-trash"> Borrar</i></button> --}}
					            </td>
					        </tr>
					    </tbody>
					</table>
				</div>
			</div>

			<div class="box-footer clearfix">
            	<pagination @setpage="getData" :param="pagination"></pagination>
    		</div>

    	</div>
    	@include('cms.ventas.partials.inputs')

    

    </section>

@endsection

@push('scripts')
    @include('cms.ventas.partials.scripts')
@endpush