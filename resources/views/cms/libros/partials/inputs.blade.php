<div class="box box-primary" v-show="panelNuevo">
	<div class="box-header">
        <h3 class="box-title">Registrar un Libro</h3>
   
        <div class="box-tools pull-right">
            <button @click="regresar" class="btn"><i class="fa fa-chevron-circle-left"></i> @lang('Regresar')</button>
            <button v-show = "botonsave" v-bind:disabled = "disable" @click="addLibro()" class="btn bg-navy"><i class="fa fa-floppy-o" ></i> Guardar
                {{-- <i v-show="loadingLibro" class="fa fa-spinner fa-spin"></i> --}}
            </button>
            <button v-show = "botonupdate" v-bind:disabled = "disable" @click="updateLibro()" class="btn bg-navy"><i class="fa fa-floppy-o"></i>Actualizar </button>

        </div>

    </div>
    {{-- loadings --}}
    <div class="text-center cargando" v-show="loading">
        <img src="../img/komvacHorizontalFlip.gif">
    </div>

    <div class="box-body">

    	<div class="col-md-6">
	    	<div class="form-group">
	            <label for="nombre">Nombre</label>
	            <input label="nombre" name="nombre" type="text" class="form-control" v-model="libro.nombre" placeholder="" :class="formError(erroresLibros, 'nombre', 'inputError')">
	        </div>

            <div class="form-group">
                    <label>Escolaridad</label>
                    <select id="tipo" name="tipo" class="form-control" v-model="libro.escolaridad" :class="formError(erroresLibros, 'escolaridad', 'inputError')">
                    <option> Todas </option>
                    <option> Secundaria </option>
                    <option> Preparatoria </option>
                    </select>
            </div>

	        <div class="form-group">
                    <label>Grado</label>
                    <select id="tipo" name="tipo" class="form-control" v-model="libro.grado" :class="formError(erroresLibros, 'grado', 'inputError')">
                        <option v-for="s in semestres" :value="s.value">@{{s.text}}</option>
                    </select>
            </div>

            <div class="form-group">
                    <label>Especialidad</label>
                    <select id="tipo" name="tipo" class="form-control" v-model="libro.especialidad">
                        <option v-for="e in especialidades" :value="e.text">@{{e.text}}</option>
                    </select>
            </div>

	        <div class="form-group">
	            <label for="stock">Stock</label>
	            <input label="stock" name="stock" type="number" class="form-control" v-model="libro.stock" placeholder="" :class="formError(erroresLibros, 'stock', 'inputError')">
	        </div>

	        <label for="precio">Precio</label><br>
	        <div class="input-group">
	            <span class="input-group-addon">$</span>
	            <input type="number" value="100" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100"  class="form-control currency" id="precio" v-model="libro.precio":class="formError(erroresLibros, 'precio', 'inputError')">
	        </div>
        </div>
        
    </div>

</div>