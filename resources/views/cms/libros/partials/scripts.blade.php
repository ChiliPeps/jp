<!-- Vue Components & Mixins -->
@include('components.vue.vuePagination')
@include('components.vue.vueHelperFunctions')
@include('components.vue.vueFormErrors')
@include('components.vue.vueNotifications')
<!-- Vue Components & Mixins -->
@include('components.vue.vueFilterSearch')

<script>
//Laravel's Token
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

var app = new Vue ({
    mounted: function () {
    	this.getData(this.dataRoute), this.getEscolaridades();
    },

    el: "#app",

    mixins: [helperFunctions, notifications],

    data: {

    	pagination:null,
    	libros:null,
    	escolaridades:null,

        botonsave:true,
        botonupdate:false,

    	loading:false,
        disable:false,
    	panelIndex:true,
    	panelNuevo:false,
    	libro:{id:'', nombre:'', grado:'', especialidad:'', stock:'', precio:'', escolaridad:''},
    	//Routes
    	dataRoute:"{{route('admin.libros.getData')}}",

        semestres:[
            {text: 'Todos',   value:'Todos'},
            {text: 'Primero', value:'1'},
            {text: 'Segundo', value:'2'},
            {text: 'Tercero', value:'3'},
            {text: 'Cuarto',  value:'4'},
            {text: 'Quinto',  value:'5'},
            {text: 'Sexto',   value:'6'},
        ],

        especialidades:[
            // {text:'',        value:''},
            {text:'Administracion', value:'Administracion'},
            {text:'Naturales',      value:'Naturales'},
            {text:'Exactas',        value:'Exactas'},
            {text:'Sociales',       value:'Sociales'},
        ],
    	//filters
    	busqueda:'',
    	tipo:'especialidad',
    	tipos:[
            { text: 'Especialidad', value:'especialidad' },
            { text: 'Nombre', value:'nombre' },
        ],

        erroresLibros:null,
        errorSql:null,
    },

    methods: {
    	getData: function (url) {
            this.loading = true;
            var filter = { busqueda: this.busqueda, tipo: this.tipo };
            var resource = this.$resource(url);
            resource.get(filter).then(function (response) {
                this.pagination = response.data;
                this.libros = response.data.data;
                this.loading = false;
            });
        },
        getEscolaridades:function(){
            var resource = this.$resource("{{route('admin.escolaridad.getData')}}");
            resource.get().then(function (response) {
                 this.escolaridades = response.data;
            });
        },

        updateFilters: function (data) {
            this.busqueda = data.busqueda;
            this.tipo = data.tipo;
            this.getData(this.dataRoute);
        },

        crearNuevo:function () {
        	this.panelIndex = false;
        	this.panelNuevo = true;
            
        },

        regresar:function(){
        	this.panelIndex = true;
        	this.panelNuevo = false;
            this.botonsave = true;
            this.botonupdate = false;
            this.erroresLibros = null;
            this.libro = {nombre:'', grado:'', especialidad:'' ,stock:'', precio:'', escolaridad:''};
        },

        addLibro: function(){
            this.disable = true
        	this.loading = true
        	var form = new FormData();
        	form.append('nombre',       		this.libro.nombre);
            form.append('escolaridad',          this.libro.escolaridad);
        	form.append('grado',                this.libro.grado);
            form.append('especialidad',         this.libro.especialidad);
        	form.append('stock',      			this.libro.stock);
        	form.append('precio',       		this.libro.precio);

        	var resource = this.$resource("{{route('admin.libro.save')}}");
        	resource.save(form).then(function (response) {

                this.loading = false
                this.disable = false
                // this.saveInAction = false;
                this.notification('fa fa-check-circle', 'OK!', "Libro Agregado!", 'topCenter');
                this.regresar();
                this.getData(this.dataRoute);
                // this.disable = false;
            }, function (response) {
                // this.loadingSaveCita = false;
                // this.saveInAction = false;
                this.erroresLibros = response.data;
                this.disable = false
                this.loading = false
                // this.disable = false;
                this.notification('fa fa-exclamation-triangle', 'Error', "Se ha producido un error.", 'topCenter');
            });
        },

        borrarLibro:function(idRecord){
           swal({
            title: '¿Estas seguro?',
            text: "Esta a punto de borrar un libro!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Si, Borrala!',
            cancelButtonText: 'Cancelar'
            }).then(function () {
                var resource = this.$resource("{{route('admin.libro.borrar')}}");
                var data = {id:idRecord}
                resource.get(data).then(function (response) {
                    console.log(response);
                    this.notification('fa fa-check-circle', 'OK!', "Libro Borrado!.", 'topCenter');
                    this.getData(this.dataRoute);
                    this.errorSql = null;
                }, function (response) {
                    console.log(response.body.message.errorInfo);
                    this.loading = false

                    if (response.body.message.errorInfo[0] == '23000')
                    {
                        this.errorSql = response.body.message.errorInfo;
                        // this.notification('fa fa-exclamation-triangle', 'Error', response.body.message.errorInfo[2], 'topCenter');
                    }
                    
                    
                });
            }.bind(this)).catch(swal.noop);
        },

        loadPanel:function(idRecord){
            this.crearNuevo();
            this.botonsave = false;
            this.botonupdate = true;


            var index = this.findIndexByKeyValue(this.libros, "id", idRecord);

            this.libro = {
                id:             this.libros[index].id,
                nombre:         this.libros[index].nombre,
                escolaridad:    this.libros[index].escolaridad,
                grado:          this.libros[index].grado,
                especialidad:   this.libros[index].especialidad,             
                stock:          this.libros[index].stock,
                precio:         this.libros[index].precio,
            }
        
            
        },
        updateLibro:function(){
            this.disable = true;
            this.loading= true;
            var form = new FormData();
            form.append('id',           this.libro.id);
            form.append('escolaridad',  this.libro.escolaridad);
            form.append('grado',        this.libro.grado);
            form.append('especialidad', this.libro.especialidad);
            form.append('nombre',       this.libro.nombre);
            form.append('stock',        this.libro.stock);
            form.append('precio',       this.libro.precio);

            var resource = this.$resource("{{route('admin.libro.update')}}");
            resource.save(form).then(function (response) {
                // this.loadingSaveCita = false;
                // this.saveInAction = false;
                this.loading = false,
                this.disable = false,
                this.notification('fa fa-check-circle', 'OK!', "Libro actualizado!", 'topCenter');
                this.regresar();
                this.getData(this.dataRoute);
            }, function (response) {
                // this.loadingSaveCita = false;
                // this.saveInAction = false;
                this.loading = false,
                this.disable = false,
                this.erroresLibros = response.data;
                this.notification('fa fa-exclamation-triangle', 'Error', "Se ha producido un error.", 'topCenter');
            });
        },

    }
});
</script>