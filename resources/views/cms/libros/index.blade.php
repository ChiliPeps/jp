@extends('cms.master')
<style>
	/*loading*/
    .cargando{
        width: 100%;
        height:100%;
        position:absolute;
        background:rgba(255,255,255,0.7);
        left:0;
        z-index: 1;
    }
</style>
@section('content')

	<section class="content-header">
        <h1><i class="fa fa-book"></i> Libros </h1>
	</section>

    <section class="content" id="app" v-cloak>
    	<div class="alert alert-danger alert-dismissible" v-if="errorSql">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
			<h4><i class="icon fa fa-ban"></i> El libro tiene ventas que no se pueden borrar</h4>
			@{{errorSql[2]}}
		</div>

        <div class="box box-primary" v-show="panelIndex">
 
        	<div class="box-header">

        		<filtersearch :selected="tipo" :options="tipos" @updatefilters="updateFilters"></filtersearch>
        		 <div class="box-tools pull-right">
                	<button  class="btn btn-block bg-navy" @click="crearNuevo"><i class="fa fa-plus-circle"></i> Crear nuevo libro</button> 
            	</div>

    		</div>

        	<div class="box-body">

				<!-- VueLoading icon -->
				<div class="text-center cargando" v-show="loading">
                        <img src="../img/komvacHorizontalFlip.gif">
                </div>

				<div class="table-responsive" > {{-- v-show="!loading" --}}
					<table class="table table-bordered table-hover">
					    <thead>
					        <tr>

					            <th>Nombre</th>
					            <th>Stock</th>
					            <th>Escolaridad</th>
					            <th>Grado</th>
					            <th>Especialidad</th>
					            <th>Precio</th>
					            <th></th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr v-for="l in libros">
					            {{-- <td><img :src="public_url+paciente.foto+'?'+Date.now()" class="img-rounded" style="width:32px; height:32px;"></td> --}}
					            <td>@{{l.nombre}}</td>
					            <td>@{{l.stock}}</td>
					            <td>@{{l.escolaridad}}</td>
					            <td>@{{l.grado}}</td>
					            <td>@{{l.especialidad}}</td>
					            <td>@{{l.precio}}</td>

					            <td>
					                <button type="button" class="btn btn-xs btn-warning" @click="loadPanel(l.id)">
					                <i class="fa fa-refresh"></i> Editar</button>
					                <button type="button" class="btn btn-xs btn-danger" @click="borrarLibro(l.id)">
					                <i class="fa fa-trash"> Borrar</i></button>
					            </td>
					        </tr>
					    </tbody>
					</table>
				</div>
			</div>
			<div class="box-footer clearfix">
                <pagination @setpage="getData" :param="pagination"></pagination>
            </div>
		</div>

		@include('cms.libros.partials.inputs')

    </section>
@endsection

@push('scripts')
    @include('cms.libros.partials.scripts')
@endpush