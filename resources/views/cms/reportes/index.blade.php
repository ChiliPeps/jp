@extends('cms.master')

<style>
    /*loading*/
    .cargando{
        width: 100%;
        height:100%;
        position:absolute;
        background:rgba(255,255,255,0.7);
        left:0;
        z-index: 1;
    }
</style>

@section('content')

    <section class="content-header">
        <h1><i class="fa fa-bar-chart"></i> Reportes </h1>
    </section>

<section class="content" id="app" v-cloak>
	<div class="box box-primary">



		<div class="nav-tabs" >
            <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Todas las ventas</a></li>
            <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Corte de caja </a></li>
            <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Libros Vendidos</a></li>
            <li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false">Alumnos Faltantes</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id ="tab_1"> 

                    @include('cms.reportes.partials.ventas')
                </div>
            <!-- /.tab-pane -->
                <div class="tab-pane " id="tab_2">
                    @include('cms.reportes.partials.corte')
                </div>
            <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_3">
                    @include('cms.reportes.partials.librosVendidos')
                </div>
            <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_4">
                    @include('cms.reportes.partials.faltantes')
                </div>
            </div>
            <!-- /.tab-content -->
    	</div>
	</div>
    
</section>

@endsection

@push('scripts')
    @include('cms.reportes.partials.scripts')
@endpush