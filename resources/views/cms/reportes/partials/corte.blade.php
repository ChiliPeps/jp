<div class="box-header">
	{{-- <datefiltersearch :selected="tipo" :options="tipos" @updatefilters="updateFilters"></datefiltersearch> --}}
	{{-- <datepicker></datepicker> --}}
	<h3>Selecciona las fecha</h3>
	{{-- <datepicker v-model="fechaCorte" v-on:change="obtenerCorte" ></datepicker> --}}
	<daterangepicker v-model = "fechasCorte" v-on:change="obtenerCorte"> </daterangepicker>
	<br>
	<button @click="obtenerCorte">Generar corte</button>
</div>

<div class="box-body">
	<!-- VueLoading icon -->
    <div class="text-center cargando" v-show="loading">
            <img src="../img/komvacHorizontalFlip.gif">
    </div>
    
	<div class="table-responsive" > {{-- v-show="!loading" --}}
		<table class="table table-bordered table-hover">
		    <thead>
		        <tr>

		            <th>ID venta</th>
		            <th>Vendedor</th>
		            <th>Total</th>
		            <th></th>
		            <th></th>
		            <th></th>
		            <th></th>
		            <th></th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr v-for="v in ventasCorte">
		            {{-- <td><img :src="public_url+paciente.foto+'?'+Date.now()" class="img-rounded" style="width:32px; height:32px;"></td> --}}

		            <td>@{{v.id}}</td>
		            <td>@{{v.vendedor}}</td>
		            <td>$@{{v.total}}</td>

		            <td>
		                <button type="button" class="btn btn-xs btn-info" @click="">
		                <i class="fa fa-file-text"></i> Detalles</button>
		            </td>
		        </tr>
		        <tr v-if = "ventasCorte.length > 0" class="success">
		        	<td></td>
		        	<td>Total</td>
		        	<td >$ @{{sumaTotal[0].total}}</td>
		        	<td>
		        		<button type="button" class="btn btn-success" @click="generarCorteSencilloPdf()">
		                <i class="fa fa fa-dollar"></i> Generar Corte</button>
		                <button type="button" class="btn btn-success" @click="generarCortePdf()">
		                <i class="fa fa fa-dollar"></i> Corte Detallado</button>
		        	</td>
		        </tr>
		    </tbody>
		</table>
	</div>
</div>