<!-- Vue Components & Mixins -->
@include('components.vue.vuePagination')
@include('components.vue.vueHelperFunctions')
@include('components.vue.vueFormErrors')
@include('components.vue.vueNotifications')
<!-- Vue Components & Mixins -->
@include('components.vue.vueDateFilterSearch')
@include('components.vue.vueFilterSearch')
@include('components.vue.vueDateRangePicker')
@include('components.vue.vueDatepicker')
<script>
//Laravel's Token
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

var app = new Vue ({
    mounted: function () {
    	this.getData(this.dataRoute), this.librosVendidos(), this.getFaltantes(this.faltantesRoute);
    },

    el: "#app",

    mixins: [helperFunctions, notifications],

    data: {

        //Data
    	pagination:'',
    	ventas:'',
        ventasCorte:'',
    	dataRoute:"{{route('admin.ventas.getData')}}",
        faltantesRoute: "{{route('admin.alumnos.faltantes')}}",
        librosventa:'',
    	//filters
    	fechas:'',
    	busqueda:'',
    	tipo:'id',
    	tipos:[
            { text: 'ID', value:'id' },
            { text: 'Nombre', value:'alumno' },
        ],

        //busquedaFaltantes
        busquedaF:'',
        tipoF:'nombre',
        tiposF:[{text:'Nombre', value:'nombre'},
                {text:'Apellido Paterno', value:'apellidoP'},
                {text:'Apellido Materno', value:'apellidoM'}
                ],
        fechaCorte:'',
        sumaTotal:'',

        fechasCorte:'',

        faltantes:null,
        loading:false,
    },

    methods: {
    	getData: function (url) {
            this.loading = true;
            var filter = { busqueda: this.busqueda, tipo: this.tipo, fechas: this.fechas};
            var resource = this.$resource(url);
            resource.get(filter).then(function (response) {
                this.pagination = response.data;
                this.ventas = response.data.data;
                this.loading = false;
            });
        },

        getFaltantes: function(url){
            //this.loading = true;
            var resource = this.$resource(url);
            var filter = { busqueda: this.busquedaF, tipo: this.tipoF};
            resource.get(filter).then(function (response) {
                this.faltantes = response.data;
                //this.loading = false;
            });
        },

        updateFilters: function (data) {
            this.busqueda = data.busqueda;
            this.tipo = data.tipo;
            this.fechas = data.fechas;
            this.getData(this.dataRoute);
            
        },

        updateFiltersF: function (data) {
            this.busquedaF = data.busqueda;
            this.tipoF = data.tipo;
            this.getFaltantes(this.faltantesRoute);
            
        },

        recuperarRecibo:function(id){
            this.loading = true;
            window.location = "{{URL::to('admin/recibo/recuperar')}}/"+id;
            this.loading = false;
        },

        obtenerCorte:function(){
            this.loading = true;
            var filter = { fechainicio:this.fechasCorte.startDate, fechafin:this.fechasCorte.endDate};
            var resource = this.$resource("{{route('admin.ventas.getCorte')}}");
            resource.get(filter).then(function (response) {
                // this.pagination = response.data;
                this.ventasCorte = response.data.corte;
                this.sumaTotal   = response.data.sum;
                this.loading     = false;
            });
        },

        generarCortePdf:function(){

            window.location = "{{URL::to('admin/corte')}}/"+this.fechasCorte.startDate+"/"+this.fechasCorte.endDate;

            //window.location = "{{URL::to('admin/corte')}}/"+this.fechaCorte;
        },

        generarCorteSencilloPdf:function(){
            window.location = "{{URL::to('admin/corteN')}}/"+this.fechasCorte.startDate+"/"+this.fechasCorte.endDate;
        },

        librosVendidos:function(){
            var resource = this.$resource("{{route('admin.libros.vendidos')}}");
            resource.get().then(function (response) {
                // this.pagination = response.data;
                this.librosventa = response.data;
                // this.sumaTotal   = response.data.sum;
                this.loading     = false;
            });
        },

        borrarVenta:function(id){
           swal({
               title: 'estas a punto de borrar una venta',
               text: 'esto no se puede revertir',
               type: 'warning',
               showCancelButton: true,
               confirmButtonColor: '#3085d6',
               cancelButtonColor: '#d33',
               confirmButtonText: 'Si, borralo',
               cancelButtonText: '{{trans("cms.cancel")}}'
               }).then(function () {
                   var resource = this.$resource("{{route('admin.venta.borrar')}}");
                   resource.delete({id:id}).then(function (response) { 
                       this.notification('fa fa-check-circle', 'OK!', 'venta borrada!', 'topCenter');
                       this.getData(this.dataRoute);
                       this.librosVendidos();
                   }, function (response) {
                       this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                   });
           }.bind(this)).catch(swal.noop);

        },

    }
});
</script>