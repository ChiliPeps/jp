<div class="box-header">
	<datefiltersearch :selected="tipo" :options="tipos" @updatefilters="updateFilters"></datefiltersearch>
</div>

<div class="box-body">

	<!-- VueLoading icon -->
	{{-- <div class="text-center"><i v-show="loading" class="fa fa-spinner fa-spin fa-5x"></i></div> --}}

	<div class="table-responsive" > {{-- v-show="!loading" --}}
		<table id="example" class="table table-bordered table-hover">
		    <thead>
		        <tr>

		            <th>ID</th>
		            <th>Nombre</th>
		            <th>Escolaridad</th>
		            <th>Vendidos</th>
		            <th>Stock</th>
		            <th></th>
		            <th></th>
		            <th></th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr v-for="l in librosventa">
		            {{-- <td><img :src="public_url+paciente.foto+'?'+Date.now()" class="img-rounded" style="width:32px; height:32px;"></td> --}}

		            <td>@{{l.id}}</td>
		            <td>@{{l.nombre}}</td>
		            <td></td>
		            <td>@{{l.num}}</td>
		            <td>@{{l.stock}}</td>
		            <td></td>
		            <td>
		                {{-- <button type="button" class="btn btn-xs btn-info" @click=""> --}}
		                {{-- <i class="fa fa-file-text"></i> </button> --}}

		            </td>
		        </tr>
		    </tbody>
		</table>
	</div>
</div>