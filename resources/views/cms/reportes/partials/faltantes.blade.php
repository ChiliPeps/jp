<div class="box-header">
	<filtersearch :selected="tipoF" :options="tiposF" @updatefilters="updateFiltersF"></filtersearch>
</div>

<div class="box-body">

	<!-- VueLoading icon -->
	{{-- <div class="text-center"><i v-show="loading" class="fa fa-spinner fa-spin fa-5x"></i></div> --}}

	<div class="table-responsive" > {{-- v-show="!loading" --}}
		<table id="example" class="table table-bordered table-hover">
		    <thead>
		        <tr>

		            <th>ID</th>
		            <th>Nombre</th>
		            <th>Apellido Paterno</th>
		            <th>Apellido Materno</th>
		            <th>Escolaridad</th>

		        </tr>
		    </thead>
		    <tbody>
		        <tr v-for="f in faltantes">
		            <td>@{{f.id}}</td>
		            <td>@{{f.nombre}}</td>
		            <td>@{{f.apellidoP}}</td>
		            <td>@{{f.apellidoM}}</td>
		            <td>@{{f.escolaridad.nivel}} @{{f.escolaridad.grado}} - @{{f.escolaridad.grupo}}</td>

		        </tr>
		    </tbody>
		</table>
	</div>
</div>