<div class="box-header">
	<datefiltersearch :selected="tipo" :options="tipos" @updatefilters="updateFilters"></datefiltersearch>
</div>

<div class="box-body">

	<!-- VueLoading icon -->
	{{-- <div class="text-center"><i v-show="loading" class="fa fa-spinner fa-spin fa-5x"></i></div> --}}

	<div class="table-responsive" > {{-- v-show="!loading" --}}
		<table class="table table-bordered table-hover">
		    <thead>
		        <tr>

		            <th>ID</th>
		            <th>Fecha</th>
		            <th>Total</th>
		            <th></th>
		            <th></th>
		            <th></th>
		            <th></th>
		            <th></th>
		            <th></th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr v-for="v in ventas">
		            {{-- <td><img :src="public_url+paciente.foto+'?'+Date.now()" class="img-rounded" style="width:32px; height:32px;"></td> --}}

		            <td>@{{v.id}}</td>
		            <td>@{{dateString(v.created_at)}}</td>
		            <td>$ @{{v.total}}</td>
		            <td>@{{v.alumno.nombre}} @{{v.alumno.apellidoP}} @{{v.alumno.apellidoM}}</td>
		            <td></td>
		            <td></td>
		            <td>
		                <button type="button" class="btn btn-xs btn-info" @click="recuperarRecibo(v.id)">
		                <i class="fa fa-file-text"></i> Recuperar recibo</button>

		            </td>
		            <td>
		            	@if(Auth::guard('cms')->user()->type == 'suadmin')
			            	<button type="button" class="btn btn-xs btn-danger" @click="borrarVenta(v.id)">
			                <i class="fa fa-trash"></i> Eliminar Venta</button>
			            @endif
		            </td>
		        </tr>
		    </tbody>
		</table>
	</div>
</div>
<div class="box-footer clearfix text-center">
    <pagination @setpage="getData" :param="pagination"></pagination>
</div>
