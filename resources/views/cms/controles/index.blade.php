@extends('cms.master')

@section('content')
    <section class="content-header">
        <h1><i class="fa fa-gears"></i> Control </h1>
    </section>

<section class="content" id="app" v-cloak>
	<div class="box box-primary">

		<div class="nav-tabs" >
            <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Cambio de escolaridad</a></li>
            <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Borrar Ventas</a></li>
            <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Excel</a></li>
            <li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false">Borrar Alumnos</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id ="tab_1"> 

                    @include('cms.controles.partials.inputs')
                </div>
            <!-- /.tab-pane -->
                <div class="tab-pane " id="tab_2">
                    @include('cms.controles.partials.ventas')
                </div>
            <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_3">
                    @include('cms.controles.partials.excel')
                </div>
            <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_4">
                    @include('cms.controles.partials.alumnos')
                </div>
            </div>
            <!-- /.tab-content -->
    	</div>
	</div>
    
</section>
@endsection

@push('scripts')
    @include('cms.controles.partials.scripts')
@endpush