<div class="box-header">
	<h3>Limpiar ventas</h3>

</div>

<div class="box-body">
	<div class="text-center cargando" v-show="loading">
	    <img src="../img/komvacHorizontalFlip.gif">
	</div>

	
	<div class="form-group">
		<label>Limpiar tabla de ventas y detalle</label><br>
		<button type="button" class="btn btn-danger btn-lg" @click="eliminarVentas">Eliminar</button>
	</div>

</div>