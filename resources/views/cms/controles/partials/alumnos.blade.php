<div class="box-header">
	<h3>Manejo de alumnos</h3>

</div>

<div class="box-body">
	<div class="text-center cargando" v-show="loading">
	    <img src="../img/komvacHorizontalFlip.gif">
	</div>

{{-- 	<div class="row">
		
		<div class="col-md-6">
			<div class="form-group">
		        <label>Escolaridad</label>
		        <select id="tipo" name="tipo" class="form-control" v-model="idEscolaridad">
		        	<option v-for="e in escolaridades" :value="e.id"> @{{e.nivel}} @{{e.grado}} @{{e.grupo}}</option>            
		        </select>
		    </div>
		    <button type="button" class="btn btn-danger btn-sm" @click="">Borrar alumnos por escolaridad</button>
		</div>

		<div class="col-md-6">
			
		</div>
	</div> --}}


	<div class="nav-tabs" >
            <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_5" data-toggle="tab" aria-expanded="true">Alumnos por escolaridad</a></li>
            <li class=""><a href="#tab_6" data-toggle="tab" aria-expanded="false">Borrar todos los alumnos</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id ="tab_5"> 
                	<div class="box-body">

	                	<div class="row">
			
							<div class="col-md-6">
								<div class="form-group">
							        <label>Seleccione una escolaridad</label>
							        <select id="tipo" name="tipo" class="form-control" v-model="idEscolaridadEliminar">
							        	<option v-for="e in escolaridades" :value="e.id"> @{{e.nivel}} @{{e.grado}} @{{e.grupo}}</option>            
							        </select>
							    </div>
							    <button type="button" class="btn btn-danger btn-sm" @click="deleteEscolaridad">Borrar alumnos por escolaridad</button>
							</div>

							<div class="col-md-6">
								
							</div>
						</div>
					</div>
 
                </div>
            <!-- /.tab-pane -->
                <div class="tab-pane " id="tab_6">
                	<div class="box-body">
                		<div class="col-md-12 text-center " style="padding-top: 60px;">
                			<button type="button" class="btn btn-danger btn-md text-center" @click="deleteAll">Borrar todos los alumnos</button>
                		</div>
                  		
                  	</div>
                </div>
            
            </div>
            <!-- /.tab-content -->
    	</div>
	

	

</div>