<div class="box-header">
	<h3>Manejo de alumnos con Excel</h3>

</div>

<div class="box-body">
	<div class="text-center cargando" v-show="loading">
	    <img src="../img/komvacHorizontalFlip.gif">
	</div>

	
	{{-- <div class="form-group">
		<label>Limpiar tabla de ventas y detalle</label><br>
		<button type="button" class="btn btn-danger btn-lg" @click="eliminarVentas">Eliminar</button>
	</div> --}}

	<div class="form-group">
		<label>Descarga el formato excel</label><br>

		<a href="{{url('alumnos.xlsx')}}" target="_blank">
        	<button target="_blank" type="button" class="btn btn-warning btn-lg">Descarga</button>
        </a>


		{{-- <button target="_blank" type="button" class="btn btn-warning btn-lg" @click="getExcel">Descarga</button> --}}
	</div>


	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label>Selecciona el archivo excel</label><br>
				<input type="file" class="form-control" accept="*" id="attachFile" name="file_attach_one" @change="getFile">
				<br>
				<button type="button" class="btn btn-success btn-sm" @click="uploadExcel">Subir</button>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
		        <label>Escolaridad</label>
		        <select id="tipo" name="tipo" class="form-control" v-model="idEscolaridad">
		        	<option v-for="e in escolaridades" :value="e.id"> @{{e.nivel}} @{{e.grado}} @{{e.grupo}}</option>            
		        </select>
		    </div>
		</div>
	</div>

	

	

</div>