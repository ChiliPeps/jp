<br>
<div class="alert alert-warning alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
	<h4><i class="icon fa fa-warning"></i> Recomendacion!</h4>
	<ul>
		<li>Se recomienda que la nueva escolaridad a la que se cambiaran los alumnos seleccionados, este vacia. (esto para evitar confisión cuando se agregen los alumnos nuevos y no aparezcan los alumnos que ya pertenecian a esa escolaridad).</li>
		<li>Se recomienda empezar con las escolaridades mayores, es decir las proximas a egresar (6tos Semestres de prepa Y 3er año de secundaria)</li>
	</ul>
	
</div>

<div class="box-header">
	<h3>Seleccione el grupo a cambiar</h3>

	<div class="form-group">
        <label>Escolaridad</label>
        <select id="tipo" name="tipo" class="form-control" v-model="escolaridad" v-on:change="getAlumnos">
        	<option v-for="e in escolaridades" :value="e.id"> @{{e.nivel}} @{{e.grado}} @{{e.grupo}}</option>            
        </select>
    </div>
</div>

<div class="box-body">

	<div class="text-center cargando" v-show="loading">
	    <img src="../img/komvacHorizontalFlip.gif">
	</div>

	<div class="row" v-show="alumnos.length > 0">
		<div class="col-md-6">
			<div class="table-responsive"  > {{-- v-show="!loading" --}}
				<table class="table table-bordered table-hover">
				    <thead>
				        <tr>

				            <th>Alumno</th>
				            {{-- <th></th> --}}
				            <th><button v-show="alumnos" @click="selectAll()">Seleccionar Todos</button></th>
				        </tr>
				    </thead>
				    <tbody>
				        <tr v-for="a in alumnos">
				            {{-- <td><img :src="public_url+paciente.foto+'?'+Date.now()" class="img-rounded" style="width:32px; height:32px;"></td> --}}
				            <td>@{{a.nombre}} @{{a.apellidoP}} @{{a.apellidoM}}</td>				           
				            {{-- <td></td> --}}

				            <td>
				                <input type="checkbox" id="checkbox" :value="a.id" v-model="seleccionados">
				            </td>
				        </tr>
				    </tbody>
				</table>
			</div>
		
		</div>

		<div class="col-md-6">
			<div class="form-group">
		        <label>Nueva Escolaridad</label>
		        <select id="tipo" name="tipo" class="form-control" v-model="nuevaEscolaridad">
		        	<option v-for="e in escolaridades" :value="e.id"> @{{e.nivel}} @{{e.grado}} @{{e.grupo}}</option>            
		        </select>
		    </div>

		    <div class="form-group" v-show="nuevaEscolaridad">
		        <label>Cambiar Seleccionados</label><br>
		        <button type="button" class="btn btn-primary btn-lg" @click="cambiarAlumnos()">Confirmar</button>
		    </div>
		</div>	
	</div>
	


</div>