<!-- Vue Components & Mixins -->
@include('components.vue.vuePagination')
@include('components.vue.vueHelperFunctions')
@include('components.vue.vueFormErrors')
@include('components.vue.vueNotifications')
<!-- Vue Components & Mixins -->

<script>
//Laravel's Token
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

var app = new Vue ({
    mounted: function () {
    	this.getEscolaridades();
    },

    el: "#app",

    mixins: [helperFunctions, notifications],

    data: {
    	loading:false,

    	nuevaEscolaridad:'',
    	escolaridad:'',
    	escolaridades:'',

    	alumnos:'',


    	seleccionados:[],

    	allSelected: false,

        archivoExcel:'',
        idEscolaridad:'',

        idEscolaridadEliminar:''
    },

    methods: {

    	getEscolaridades:function(){
            var resource = this.$resource("{{route('admin.escolaridad.getData')}}");
            resource.get().then(function (response) {
                 this.escolaridades = response.data;
            });
        },

        getAlumnos: function () {
            this.loading = true;
            var datos = { id_escolaridad: this.escolaridad };
            var resource = this.$resource("{{route('admin.control.getAlumnos')}}");
            resource.get(datos).then(function (response) {
                // this.pagination = response.data;
                this.alumnos = response.data;
                this.loading = false;
            });
        },

        selectAll: function() {
            this.seleccionados = [];

            // if (!this.allSelected) {
                for (user in this.alumnos) {
                    this.seleccionados.push(this.alumnos[user].id);
                // }
            }
        },

        cambiarAlumnos:function(){
        	this.loading = true
        	var datos = {alumnos:this.seleccionados, escolaridadActual:this.escolaridad, nuevaEscolaridad:this.nuevaEscolaridad}
        	var resource = this.$resource("{{route('admin.control.updateEscolaridad')}}");
            resource.save(datos).then(function (response) {

                this.loading = false
                this.notification('fa fa-check-circle', 'OK!', "Alumnos actualizados", 'topCenter');
                this.limpiar();

            }, function (response) {
                this.loading = false
                this.notification('fa fa-exclamation-triangle', 'Error', "Se ha producido un error.", 'topCenter');
            });
        },

        limpiar:function(){
        	this.escolaridad = ''
        	this.seleccionados =[]
        	this.nuevaEscolaridad = ''
        	this.alumnos = ""
        },

        // eliminar ventas

        eliminarVentas:function(){
        	swal({
            title: '¿Estas seguro?',
            text: "Esta a punto de borrar las ventas!, no se puede deshacer",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Si, Borralas!',
            cancelButtonText: 'Cancelar'
            }).then(function () {
                var resource = this.$resource("{{route('admin.control.borrar')}}");
                // var data = {id:idRecord}
                resource.get().then(function (response) {
                    this.notification('fa fa-check-circle', 'OK!', "Ventas borradas del sistema.", 'topCenter');
                    // this.getData(this.dataRoute);
                });
            }.bind(this)).catch(swal.noop);
        },

        getExcel:function(){
            var resource = this.$resource("{{route('admin.control.getExcel')}}");
            resource.get().then(function (response) {
            });
        },

        uploadExcel:function(){
            if(this.archivoExcel == "" || this.idEscolaridad== ""){
                 this.notification('fa fa-check-circle', 'OK!', "debes Cargar el excel o la escolaridad", 'topCenter');
            }

            else{
                var form = new FormData();
                form.append('archivo', this.archivoExcel);
                form.append('idEscolaridad', this.idEscolaridad);
                this.loading = true
                var resource = this.$resource("{{route('admin.control.subir')}}");
                resource.save(form).then(function (response) {
                    this.loading = false
                    this.notification('fa fa-check-circle', 'OK!', "Alumnos actualizados", 'topCenter');
                    this.idEscolaridad = ""
                    this.archivoExcel = ""
                    $("#attachFile").val('');
                });
            }

        },

        getFile:function (e) {
          var files = e.target.files || e.dataTransfer.files
          if (!files.length)
            return;

            if(!this.isValidFile(files)) {
                attachFile.value = "";
                return;
            }
            // console.log('hola' + files[0]);
            this.archivoExcel = files[0];
          // this.createImage(files[0]);
        },

        isValidFile: function (files) {
            var _validFileExtensions = ["jpg", "jpeg", "png", "pdf", "xlsx"];
            for (var i = 0; i < files.length; i++) {

                //Size
                if (files[i].size > 2097152) { 
                    this.notification('fa fa-exclamation-triangle', 'Error', "La imagen es mayor a 2 Mb.", 'topCenter');
                    return false 
                } //2 MB

                //Type
                type = files[i].name.split('.')[files[i].name.split('.').length - 1].toLowerCase()
                if (_validFileExtensions.indexOf(type) == -1) { 
                    this.notification('fa fa-exclamation-triangle', 'Error', "Extensión de archivo incorrecta. [jpg, jpeg, png, pdf, xlsx]", 'topCenter');
                    return false 
                }

            } return true
        },

        deleteAll:function(){
            swal({
            title: '¿Estas seguro?',
            text: "Se borraran todas las ventas relacionadas con los alumnos a eliminar",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Si, Borra TODOS los alumnos!',
            cancelButtonText: 'Cancelar'
            }).then(function () {
                var resource = this.$resource("{{route('admin.control.borrarTodosAlumnos')}}");
                // var data = {id:idRecord}
                resource.get().then(function (response) {
                    this.notification('fa fa-check-circle', 'OK!', "Todos los alumnos borrados.", 'topCenter');
                    // this.getData(this.dataRoute);
                });
            }.bind(this)).catch(swal.noop);
        },

        deleteEscolaridad:function(){
            if (this.idEscolaridadEliminar == "") {
                this.notification('fa fa-check-circle', 'Error', "Debes seleccionar una escolaridad.", 'topCenter');
            }
            else{
                swal({
                title: '¿Estas seguro?',
                text: "Se borraran todas las ventas relacionadas con los alumnos a eliminar",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Si, Borra los alumnos!',
                cancelButtonText: 'Cancelar'
                }).then(function () {
                    var resource = this.$resource("{{route('admin.control.borrarAlumnosEscolaridad')}}");
                    var data = {id:this.idEscolaridadEliminar}
                    resource.get(data).then(function (response) {
                        this.notification('fa fa-check-circle', 'OK!', "Alumnos borrados por escolaridad.", 'topCenter');
                    });
                }.bind(this)).catch(swal.noop);
            }
        }

    }
});
</script>