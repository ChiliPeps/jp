@extends('cms.master')

@section('content')
    <section class="content-header" id="app">
        <h1><i class="fa fa-file"></i> Escolaridades </h1>
    </section>
@endsection

@push('scripts')
    @include('cms.escolaridades.partials.scripts')
@endpush