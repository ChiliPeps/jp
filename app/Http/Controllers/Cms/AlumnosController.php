<?php

namespace App\Http\Controllers\CMS;

use Carbon\Carbon;
use App\Models\CMS\CMSAlumno;
use App\Models\CMS\CMSVenta;
use App\Http\Requests\CMS\CMSAlumnosRequest;
use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AlumnosController extends Controller
{
    public function __construct() 
    {
        $this->middleware('CMSAuthenticate');
    }

    public function index()
    {
        return view('cms.alumnos.index');
    }

    public function getAlumnos(Request $request)
    {

    	if($request->has('tipo') && $request->has('busqueda')) {

            $tipo = $request->input('tipo');
            $busqueda = $request->input('busqueda');

            $results = CMSAlumno::with('escolaridad')->where($tipo, 'LIKE', $busqueda.'%')
            ->orderBy('created_at', 'desc')->paginate(20);

        } else {
            $results = CMSAlumno::with('escolaridad')->orderBy('created_at', 'desc')->paginate(20);
        }

        return response()->json($results);
    }

    public function saveAlumno(CMSAlumnosRequest $request)
    {
        $alumno = CMSAlumno::create([         
            'nombre'            => $request->input('nombre'),
            'id_escolaridad'    => $request->input('id_escolaridad'),
            'apellidoP'         => $request->input('apellidoP'),
            'apellidoM'         => $request->input('apellidoM')
            ]);
        $alumno->save();
    }

    public function updateAlumno(CMSAlumnosRequest $request)
    {   
        // dd($request->input('apellidoM'));
        // http_response_code(500);
        // return false;
        $alumno = CMSAlumno::findOrFail($request->input('id'));

        //Update Cita
        $alumno->fill([
            'nombre'            => $request->input('nombre'),
            'id_escolaridad'    => $request->input('id_escolaridad'),
            'apellidoP'         => $request->input('apellidoP'),
            'apellidoM'         => $this->checkNull($request->input('apellidoM'))
        ]);

        // dd($alumno->apellidoM);
        $alumno->save();
    }

    public function borrarAlumno(Request $request)
    {
        $alumno = CMSAlumno::findOrFail($request->input('id'));
        $alumno->delete();
    }

    public function faltantes(Request $request)
    {   
        $alumnosVentas = CMSVenta::select('id_alumno')->distinct()->get();

        if($request->has('tipo') && $request->has('busqueda')) {

            $tipo = $request->input('tipo');
            $busqueda = $request->input('busqueda');

            $alumnosFaltantes = CMSAlumno::with('escolaridad')->whereNotIn('id', $alumnosVentas)->where($tipo, 'LIKE', $busqueda.'%')
            ->orderBy('created_at', 'desc')->get();

        } else {
            $alumnosFaltantes = CMSAlumno::with('escolaridad')->whereNotIn('id', $alumnosVentas)->get();
        }

        return response()->json($alumnosFaltantes);
    }

    protected function checkNull($value) {
        if ($value == "null") { return ""; }
        else { return $value; }
    }
}
