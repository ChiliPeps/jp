<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CMS\CMSLibro;
use App\Http\Requests\CMS\CMSLibrosRequest;
Use Exception;


class LibrosController extends Controller
{
    public function __construct() 
    {
        $this->middleware('CMSAuthenticate');
    }

    public function index()
    {
        return view('cms.libros.index');
    }

    public function getAllLibros(Request $request)
    {
    	if($request->has('tipo') && $request->has('busqueda')) {

            $tipo = $request->input('tipo');
            $busqueda = $request->input('busqueda');

            $results = CMSLibro::where($tipo, 'LIKE', $busqueda.'%')
            ->orderBy('created_at', 'desc')->paginate(20);

        } else {
            $results = CMSLibro::orderBy('created_at', 'desc')->paginate(20);
        }

        return response()->json($results);
    }

    public function saveLibro(CMSLibrosRequest $request)
    {
    	$libro = CMSLibro::create([    		
    		'nombre'		=> $request->input('nombre'),
            'escolaridad'   => $request->input('escolaridad'),
    		'grado'         => $request->input('grado'),
            'especialidad'  => $request->input('especialidad'),
    		'stock'			=> $request->input('stock'),
    		'precio' 		=> $request->input('precio')
    		]);
    	$libro->save();
    }

    public function borrarLibro(Request $request)
    {
        try {
            $libro = CMSLibro::findOrFail($request->input('id'));
            $libro->delete();
        } 
        catch (\PDOException $e) {
            return response()->json(['message' => $e], 500);
        }
    }

    public function updateLibro(CMSLibrosRequest $request)
    {
        $libro = CMSLibro::findOrFail($request->input('id'));
        if ( $request->input('especialidad') == 'undefined' ) {

            $libro->fill([
            'nombre'        => $request->input('nombre'),
            'escolaridad'   => $request->input('escolaridad'),
            'grado'         => $request->input('grado'),
            'especialidad'  => NULL,
            'stock'         => $request->input('stock'),
            'precio'        => $request->input('precio')
            ]);
        }
        else{
            $libro->fill([
                'nombre'        => $request->input('nombre'),
                'escolaridad'   => $request->input('escolaridad'),
                'grado'         => $request->input('grado'),
                'especialidad'  => $request->input('especialidad'),
                'stock'         => $request->input('stock'),
                'precio'        => $request->input('precio')
            ]);
        }

        $libro->save();
    }
}
