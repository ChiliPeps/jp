<?php

namespace App\Http\Controllers\CMS;

use Carbon\Carbon;
use Jenssegers\Date\Date;

use App\Models\CMS\CMSVenta;
use App\Models\CMS\CMSVentadetalle;
use App\Models\CMS\CMSAlumno;
use App\Models\CMS\CMSLibro;
use App\Models\CMS\CMSEscolaridad;
use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use Response;
use App\Http\Controllers\Controller;
use DB;
use PDF;

class VentasController extends Controller
{
    public function __construct() 
    {
        $this->middleware('CMSAuthenticate');
    }

    public function index()
    {
        
        return view('cms.ventas.index');

    }

    //metodo para que no aparezcan los alumnos con los libros adquiridos
    public function getAlumnosVender(Request $request)
    {
        $alumnosvendidos = CMSVenta::select('id_alumno')->get();

    	if($request->has('tipo') && $request->has('busqueda')) {

            $tipo = $request->input('tipo');
            $busqueda = $request->input('busqueda');

            $results = CMSAlumno::whereNotIn('id', $alumnosvendidos)->with('escolaridad')->where($tipo, 'LIKE', $busqueda.'%')
            ->orderBy('created_at', 'desc')->paginate(20);

        } else {
            $results = CMSAlumno::with('escolaridad')->orderBy('created_at', 'desc')->paginate(20);
        }

        return response()->json($results);
    }
//--------------------------------------------------------------------------------------

    public function getLibros(Request $request)
    {         
        $idescolaridad = $request->input('id');
        $grado = CMSEscolaridad::where('id',$idescolaridad)->pluck('grado');
        $escolaridad = CMSEscolaridad::where('id',$idescolaridad)->pluck('nivel');
        $grupo = CMSEscolaridad::where('id',$idescolaridad)->pluck('grupo');        

        if($grupo == '["A"]' && $escolaridad == '["Preparatoria"]' && $grado == '["5"]')
        {
            $esp='Administracion';
        }
        else if ($grupo == '["B"]'&& $escolaridad == '["Preparatoria"]' && $grado == '["5"]')
            { $esp = 'Naturales';}
        else if ($grupo == '["C"]' && $escolaridad == '["Preparatoria"]' && $grado == '["5"]')
            { $esp = 'Exactas';}
        else if ($grupo == '["D"]' && $escolaridad == '["Preparatoria"]' && $grado == '["5"]')
            { $esp = 'Sociales';}
        else{$esp = 0;}

        

        if($request->has('tipo') && $request->has('busqueda')) {
            
            $tipo = $request->input('tipo');
            $busqueda = $request->input('busqueda');

            // $libros = CMSLibro::where($tipo, 'LIKE', $busqueda.'%')->where('grado', $grado)
            // ->paginate(15);

            $libros=CMSLibro::where([
                [$tipo, 'LIKE', $busqueda.'%'],
                ['grado', $grado],
                ['escolaridad', $escolaridad],
                ['especialidad', $esp]
            ])->Orwhere([
                [$tipo, 'LIKE', $busqueda.'%'],
                ['grado', $grado],
                ['escolaridad', $escolaridad],
                ['especialidad', null]
            ])->orWhere('grado', 'todos')->orderBy('created_at', 'desc')->paginate(15);

        }
        else{

            $libros = CMSLibro::where([
                ['grado', $grado],
                ['escolaridad', $escolaridad],
                ['especialidad', $esp]
            ])->Orwhere([
                ['grado', $grado],
                ['escolaridad', $escolaridad],
                ['especialidad', null]
            ])->orWhere('grado', 'todos')->orderBy('created_at', 'desc')->paginate(15);
        }
        $librosEsp = CMSLibro::where('especialidad', $esp)->get();
    	return response()->json($libros);


    }

    public function getVentas(Request $request)
    {       

        if($request->has('tipo') && $request->has('busqueda')) {
            
            $tipo = $request->input('tipo');
            $busqueda = $request->input('busqueda');
            if($request->has('fechas'))
                {
                     $ventas = CMSVenta::where($tipo, 'LIKE', $busqueda.'%')->whereBetween('fecha', [$fecha['startDate'], $fecha['endDate']])
                     ->orderBy('created_at', 'desc')->paginate(20);
                }

            else{

                    $ventas = CMSVenta::with('alumno')->where($tipo, $busqueda)->orderBy('created_at', 'desc')->paginate(20);
                }
        }
        else{

            $ventas = CMSVenta::with('alumno')->orderBy('created_at', 'desc')->paginate(20);
        }

        return response()->json($ventas);
    }


    public function ventaLibro(Request $request)
    {
        $hoy = Carbon::now();
        $total    = $request->input('total');
        $idalumno = $request->input('id');
        $efectivo = $request->input('efectivo');
        $nombreuser =  Auth::guard('cms')->user()->name;

        $listaA = $request->input('lista');
        $listaInsert = array();

        //insert en tabla venta
        $venta = CMSVenta::create([
            'id_alumno' => $idalumno,
            'vendedor'   => $nombreuser,
            'total'    => $total,
            'pagoCon'  => $efectivo
            ]);
        $venta->save();

        //insert en tabla venta_detalle
        foreach ($listaA as $l) {
           $s['id_libro']       = $l['id'];
           CMSLibro::where('id', $l['id'])->decrement('stock');
           $s['precio_libro']   = $l['precio'];
           $s['id_venta']       = $venta->id;
           $listaInsert[]       = $s;  
        }
        DB::table('cms_venta_detalle')->insert($listaInsert);

        return $venta->id;
    }

    public function generarPDF($id){

        $hoy = $this->getDateInSpanishFormat(Carbon::today());

        $datosVenta = CMSVentadetalle::with('libro')->where('id_venta', $id)->get()->toArray();

        $venta = CMSVenta::with('alumno.escolaridad')->where('id',$id)->get()->toArray();

        $data['datos'] = $datosVenta;
        $data['venta'] = $venta;
        $data['fechaEsp'] = $hoy;
        $data['idVenta'] = $id;


        $pdf = PDF::loadView('pdf.recibo', ['data' => $data] );
        return $pdf->download('recibo'.$id.'.pdf');

    }

    public function recuperarPdf($id)
    {
        $datosVenta = CMSVentadetalle::with('libro')->where('id_venta', $id)->get()->toArray();
        $venta = CMSVenta::with('alumno.escolaridad')->where('id',$id)->get()->toArray();        

        //recuperar fecha de la venta y ponerla con formato
        $fecha = $venta[0]['created_at'];
        $fechaEsp = $this->getDateInSpanishFormat($fecha);
        $data['datos'] = $datosVenta;
        $data['venta'] = $venta;
        $data['idVenta'] = $id;
        $data['fechaEsp'] = $fechaEsp;
        // dd($data);
        $pdf = PDF::loadView('pdf.recibo2', ['data' => $data] );
        return $pdf->download('recibo'.$id.'.pdf');
    }

    public function getCorte(Request $request)
    {
        $fechaInicio= $request->input('fechainicio');
        $fechaFin   = $request->input('fechafin');


        //$corte = CMSVenta::with('detalle.libro')->where('created_at', 'LIKE', $fecha.'%')->get();
        //$suma = CMSVenta::where('created_at', 'LIKE', $fecha.'%')->selectRaw('sum(total) as total')->get();

        $corte = CMSVenta::with('detalle.libro')->whereBetween('created_at', [$fechaInicio." 00:00:00", $fechaFin." 23:59:59"])->get();

        $suma = CMSVenta::whereBetween('created_at', [$fechaInicio." 00:00:00", $fechaFin." 23:59:59"])->selectRaw('sum(total) as total')->get();

        return response()->json(['corte'=>$corte , 'sum'=>$suma]);
    }

    public function deleteVenta(Request $request)
    {
        $idVenta = $request->input('id');

        $venta = CMSVenta::findOrFail($request->input('id'));
        
        $venta_detalle = CMSVentadetalle::where('id_venta', $idVenta)->get();
        for ($i=0; $i < count($venta_detalle); $i++) { 

           CMSLibro::where('id',$venta_detalle[$i]->id_libro)->increment('stock');
           $detalle = CMSVentadetalle::where('id', $venta_detalle[$i]->id);
           $detalle->delete();
        }

        $venta->delete();

    }

    protected function getDateInSpanishFormat($fecha)
    {
        Date::setLocale('es');
        $date = new Date($fecha);
        return $date->format('l j F Y');
        // return $date->format('y/m/d');
    }
}
