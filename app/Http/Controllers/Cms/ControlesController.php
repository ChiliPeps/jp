<?php

namespace App\Http\Controllers\CMS;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\CMS\CMSAlumno;
use App\Models\CMS\CMSVenta;
use App\Models\CMS\CMSVentadetalle;
use App\Models\CMS\CMSLibro;
use DB;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;


class ControlesController extends Controller
{
    public function __construct() 
    {
        $this->middleware('IsAdmin');
    }

    public function index()
    {
        return view('cms.controles.index');
    }

    public function getAlumnos(Request $request){

    	$id = $request->input('id_escolaridad');
    	// dd($id);
        $results = CMSAlumno::with('escolaridad')->where('id_escolaridad', $id)->get();
        

        return response()->json($results);
    }

    public function updateEscolaridades(Request $request){
    	$escolaridadActual  = $request->input('escolaridadActual');
    	$nuevaEscolaridad = $request->input('nuevaEscolaridad');

    	// $alumnos = array();
    	$alumnos = $request->input('alumnos');

    	$query = CMSAlumno::WhereIn('id',$alumnos)->update(['id_escolaridad' => $nuevaEscolaridad]);
    }

    public function borrarVentas(){
    	DB::statement('SET FOREIGN_KEY_CHECKS=0;');
    	CMSVentadetalle::truncate();
    	CMSVenta::truncate();
    	DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }

    public function getExcel(){
        

    }
    
    public function uploadExcel(Request $request)
    {
        $inputFileName = $request->file('archivo');
        $id_escolaridad = $request->input('idEscolaridad');

        $spreadsheet = IOFactory::load($inputFileName);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        // dd($sheetData);

        for ($i = 5; $i <= count($sheetData); $i++) {
            $nombre    = $sheetData[$i]['A'];
            $apellidoP = $sheetData[$i]['B'];
            $apellidoM = $sheetData[$i]['C'];
            
            // Save Alumno
            if($nombre) 
            {                
                $alumno = CMSAlumno::create([         
                    'nombre'            => $nombre,
                    'apellidoP'         => $apellidoP,
                    'apellidoM'         => $apellidoM,
                    'id_escolaridad'    => $id_escolaridad
                    ]);
                $alumno->save();
            }
        }
        return response()->json(['success' => true]);

    }

    public function borrarTodosAlumnos(){
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        CMSVentadetalle::truncate();
        CMSVenta::truncate();
        CMSAlumno::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        return response()->json(['success' => true]);
    }

    public function borrarAlumnosEscolaridad(Request $request){
        $id_escolaridad = $request->input('id');
        
        $alumnos = CMSAlumno::where('id_escolaridad', $id_escolaridad)->get();
        $alumnosIds = CMSAlumno::select('id')->where('id_escolaridad', $id_escolaridad)->get()->toArray();
        
            $venta = CMSVenta::whereIn('id_alumno', $alumnosIds)->get();
            $ventaIds = CMSVenta::select('id')->whereIn('id_alumno', $alumnosIds)->get()->toArray();

                $ventaDetalle = CMSVentadetalle::whereIn('id_venta', $ventaIds)->get();

                    for ($i=0; $i < count($ventaDetalle); $i++) { 

                           CMSLibro::where('id',$ventaDetalle[$i]->id_libro)->increment('stock');
                           $detalle = CMSVentadetalle::where('id', $ventaDetalle[$i]->id);
                           $detalle->forceDelete();
                        }

                // $ventaDetalle->each->forceDelete();

            $venta->each->forceDelete();
        $alumnos->each->delete();  


        return response()->json(['success' => true]);
        
    }
}
