<?php

namespace App\Http\Controllers\CMS;

use Jenssegers\Date\Date;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CMS\CMSVenta;
use App\Models\CMS\CMSLibro;
use App\Models\CMS\CMSVentadetalle;
use PDF;
use DB;

class ReportesController extends Controller
{
    public function __construct() 
    {
        $this->middleware('CMSAuthenticate');
    }

    public function index()
    {
        return view('cms.reportes.index');
    }

    public function corteDetallePdf($fecha, $fecha2)
    {
    	$fechaSpanish = $this->getDateInSpanishFormat($fecha);
        $fechaSpanish2 = $this->getDateInSpanishFormat($fecha2);
    	$corte = CMSVenta::with(['alumno','detalle.libro'])->whereBetween('created_at', [$fecha." 00:00:00", $fecha2." 23:59:59"])->get()->toArray();
        $suma = CMSVenta::whereBetween('created_at', [$fecha." 00:00:00", $fecha2." 23:59:59"])->selectRaw('sum(total) as total')->get();
        // $alumno = CMSVenta::with('alumno.escolaridad')->where('id',$id)->get()->toArray();

        $data['corte'] = $corte;
        //dd($corte);
        $data['suma'] = $suma;
        $data['fechaEsp'] = $fechaSpanish;
        $data['fechaEsp2'] = $fechaSpanish2;

         // dd($corte, $suma, $fechaSpanish);
        $pdf = PDF::loadView('pdf.corteDetalle', ['data' => $data] );
        return $pdf->download('corte detalle- '.$fechaSpanish.' - '.$fechaSpanish2.'.pdf');

    }
    public function cortePdf($fecha, $fecha2)
    {
        $fechaSpanish = $this->getDateInSpanishFormat($fecha);
        $fechaSpanish2 = $this->getDateInSpanishFormat($fecha2);
        //$corte = CMSVenta::with(['alumno','detalle.libro'])->where('created_at', 'LIKE', $fecha.'%')->get();
        //$suma = CMSVenta::where('created_at', 'LIKE', $fecha.'%')->selectRaw('sum(total) as total')->get();
        $corte = CMSVenta::with(['alumno','detalle.libro'])->whereBetween('created_at', [$fecha." 00:00:00", $fecha2." 23:59:59"])->get();
        $suma = CMSVenta::whereBetween('created_at', [$fecha." 00:00:00", $fecha2." 23:59:59"])->selectRaw('sum(total) as total')->get();
        

        $data['corte'] = $corte;
        $data['suma'] = $suma;
        $data['fechaEsp'] = $fechaSpanish;
        $data['fechaEsp2']= $fechaSpanish2;

         // dd($corte, $suma, $fechaSpanish);
        $pdf = PDF::loadView('pdf.corte', ['data' => $data] );
        return $pdf->download('corte - '.$fechaSpanish.' - '.$fechaSpanish2.'.pdf');

    }

    public function librosVendidos()
    {
        $libros = DB::select('SELECT L.id,  L.nombre, count(id_libro) as num, stock from cms_venta_detalle V, cms_libros L where (L.id = V.id_libro ) and (V.deleted_at is null) GROUP by L.nombre, stock, L.id');
        return response()->json($libros);
    }
     protected function getDateInSpanishFormat($fecha)
    {
        Date::setLocale('es');
        $date = new Date($fecha);
        return $date->format('l j F Y');
        // return $date->format('y/m/d');
    }
}
