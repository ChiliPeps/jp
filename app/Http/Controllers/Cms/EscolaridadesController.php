<?php

namespace App\Http\Controllers\CMS;

use App\Models\CMS\CMSEscolaridad;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EscolaridadesController extends Controller
{
    public function __construct() 
    {
        $this->middleware('CMSAuthenticate');
    }

    public function index()
    {
        return view('cms.escolaridades.index');
    }

    public function getAll(Request $request){

    	if($request->has('tipo') && $request->has('busqueda')) {

            $tipo = $request->input('tipo');
            $busqueda = $request->input('busqueda');

            $results = CMSEscolaridad::where($tipo, 'LIKE', $busqueda.'%')
            // ->orderBy('created_at', 'desc')->paginate(20);
            ->orderBy('created_at', 'desc')->get();

        } else {
            $results = CMSEscolaridad::orderBy('created_at', 'desc')
            // ->paginate(20);
            ->get();
        }
        return response()->json($results);
    }
}
