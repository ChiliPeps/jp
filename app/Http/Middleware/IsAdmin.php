<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('cms')->user()->type == 'suadmin' || Auth::guard('cms')->user()->type == 'admin')
            return $next($request);                  
        
        return redirect('/admin');
        
    }
}
