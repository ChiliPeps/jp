<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CMSVentadetalle extends Model
{
    use SoftDeletes;
     /**
     * The table associated with the model.
     *
     * @var string
     */

    protected $table = 'cms_venta_detalle';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','id_venta','id_libro','precio_libro'];


    public function libro()
    {
        return $this->belongsTo('App\Models\CMS\CMSLibro', 'id_libro');
    }

    public function venta()
    {
        return $this->belongsTo('App\Models\CMS\CMSVenta', 'id_venta');
    }
}
