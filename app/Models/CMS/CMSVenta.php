<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CMSVenta extends Model
{
    use SoftDeletes;

    protected $table = 'cms_ventas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','id_alumno','vendedor','total', 'pagoCon' ];


     public function alumno()
    {
        return $this->belongsTo('App\Models\CMS\CMSAlumno', 'id_alumno');
    }

    public function detalle()
    {
        return $this->hasMany('App\Models\CMS\CMSVentadetalle', 'id_venta');
    }
}
