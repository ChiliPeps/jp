<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;

class CMSLibro extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_libros';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre', 'grado', 'especialidad', 'precio', 'stock', 'escolaridad'];

}
