<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;

class CMSAlumno extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_alumnos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre','apellidoP','apellidoM','id_escolaridad'];


    public function escolaridad()
    {
        return $this->belongsTo('App\Models\CMS\CMSEscolaridad', 'id_escolaridad');
    }
}