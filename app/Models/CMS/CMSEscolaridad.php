<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;

class CMSEscolaridad extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_escolaridades';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nivel','grado','grupo'];


}
