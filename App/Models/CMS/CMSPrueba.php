<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;

class CMSPrueba extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_pruebas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ ];
}
