<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;

class CMSControl extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_controles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ ];
}
