<?php

return [
    'user_types' => [
        'suadmin' => 'Super admin',
    ],
    'update_password'       => 'Actualizar contraseña',
    'block_user'            => 'Bloquear usuario',
    'unblock_user'          => 'Desbloquear usuario',
    'user_blocked'          => 'Usuario bloqueado',
    'update_my_password'    => 'Actualizar mi contraseña',
    'edit_user'             => 'Editar usuario',
    'users'                 => 'Usuarios',
    'create_new_user'       => 'Crear usuario nuevo',
    'msg_user_created'      => 'Usuario creado!',
    'msg_user_updated'      => 'Usuario actualizado!',
    'msg_you_cant_delete_yourself' => 'No puedes borrarte a ti mismo!',
    'msg_password_updated'  => 'Contraseña actualizada!',
    'msg_user_blocked'      => 'Usuario bloqueado!',
    'msg_user_unblocked'    => 'Usuario desbloqueado!',
    'msg_you_cant_block_yourself' => 'No puedes bloquearte a ti mismo!',
    'msg_user_deleted' => 'Usuario borrado!',
];