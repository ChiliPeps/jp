<?php

return [

    'app_name' => 'JPII',

    'template_skin' => 'blue', // blue, black, purple, yellow, red, green

    'template_layout_options' => ['fixed'], // fixed, layout-boxed, layout-top-nav, sidebar-collapse, sidebar-mini, sidebar-mini sidebar-collapse

    // Users Roles
    'user_types' => ['suadmin', 'admin', 'editor'],

    // Media manager
    'default_path' => 'media-manager',

    // Login Background
    'login_background_url' => '', //URL goes -> '../img/cms/back.jpg'
];